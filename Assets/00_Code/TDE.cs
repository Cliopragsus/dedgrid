﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDE : MonoBehaviour
{
    public enum SpeechState
    {
        Mute,
        Parallel,
        Attention,
    }
    public SpeechState currentSpeechState = SpeechState.Mute;
    public SpeechState lastSpeechState;

    public enum GameplayResponseType
    {
        Command,
        Ability,
        UnitDeath,
    }

    private AudioSource aS;
    public GameObject tde;
    private GameManager gm;
    private VoiceLineContainer vlc;
    private Animator anim;

    public string currentClip;
    public string followUpClip;
    public float dialogueDuration;
    public float dialogueCounter;

    private List<string> GameplayResponses_UnitDeath = new List<string>(){
        "tragic_00", "greaterGood_00", "poeticEnd_00"
    };

    private List<string> intros = new List<string>(){
        "intro_00", "intro_00_Abort" 
    };

    private List<string> animationClips = new List<string>(){
        "Talk_00", "Talk_01", "Talk_02", "Laugh_00", "Laugh_01", "Bow_00", "Point_00", "Sway_00", "Sway_01" 
    };

    private List<string> specifics = new List<string>();

    private List<string> memories = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        aS = tde.GetComponent<AudioSource>();
        gm = GetComponent<GameManager>();
        vlc = GetComponent<VoiceLineContainer>();
        anim = tde.transform.GetChild(0).GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //tde.transform.position = Vector3.MoveTowards(tde.transform.position, gm.cb.eye.transform.position - (gm.cb.eye.transform.position - tde.transform.position).normalized * 0.5f - (gm.cb.eye.transform.position - gm.targeter.transform.position).normalized * 0.5f, 1 * Time.deltaTime);
        //tde.transform.LookAt(gm.cb.eye.transform.position);

        if (aS.isPlaying)
        {
            AnimationSync();
        }
        dialogueCounter += Time.deltaTime;

        switch (currentSpeechState)
        {
            case SpeechState.Attention:

                if (Input.GetMouseButtonDown(1))
                {
                    SetSpeechState(SpeechState.Parallel);
                    gm.RequestControlStateChange(gm.lastControlState);
                    PlayClip(followUpClip);
                    return;
                }

                if (dialogueCounter >= dialogueDuration)
                {
                    SetSpeechState(SpeechState.Mute);
                    gm.RequestGameStateChange(gm.lastGameState);
                    return;
                }
                break;

            case SpeechState.Parallel:
                if (dialogueCounter >= dialogueDuration)
                {
                    SetSpeechState(SpeechState.Mute);
                }
                //
                break;
        }
    }

    public void AnimationSync()
    {
        switch (currentClip)
        {
            case "intro_00":
                if (AnimationSyncWindow(4))
                {
                    anim.Play("Point_00");
                }
                break;

            case "intro_01":
                //
                break;
        }
    }

    public bool AnimationSyncWindow(float t)
    {
        if (dialogueCounter > t && dialogueCounter < t + 0.1f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetSpeechState(SpeechState desiredState)
    {
        if (desiredState == SpeechState.Attention)
        {
            //gm.cb.SetObjectofInterest(tde);
            gm.RequestGameStateChange(GameManager.GameState.Transition);
        }
        if (desiredState == SpeechState.Parallel)
        {
            if (currentSpeechState == SpeechState.Attention)
            {
                gm.cb.SetObjectofInterest(gm.targeter);
                if (followUpClip != "none")
                {
                    PlayClip(followUpClip);
                }
                followUpClip = "none";
            }
        }
        if (desiredState == SpeechState.Mute && currentSpeechState == SpeechState.Attention)
        {
            gm.cb.SetObjectofInterest(gm.targeter);
        }
        lastSpeechState = currentSpeechState;
        currentSpeechState = desiredState;
        Debug.Log("SpeechState changed from " + lastSpeechState + " to " + desiredState);
    }

    public void StartDialogue()
    {
        SetSpeechState(SpeechState.Attention);
    }

    public void PlayClip(string clipName) // if the abort reaction is important, insert a new StartDialogue()
    {
        switch (clipName)
        {
            // Intro ---------------------------------------
            case "intro_00":
                aS.clip = vlc.intro_00;
                followUpClip = "intro_00_Abort";
                break;

            case "intro_00_Abort":
                aS.clip = vlc.intro_00_Abort;
                break;

            case "intro_01":
                aS.clip = vlc.intro_01;
                break;

                // Death Responses --------------------------

            case "poeticEnd_00":
                aS.clip = vlc.poeticEnd_00;
                break;

            case "tragic_00":
                aS.clip = vlc.tragic_00;
                break;

            case "greaterGood_00":
                aS.clip = vlc.greaterGood_00;
                break;
        }
        anim.Play(animationClips[(int)Random.Range(0, animationClips.Count - 1)]);
        Debug.Log("VoiceLine : " + clipName);
        currentClip = clipName;
        if (currentClip != "none")
        {
            dialogueDuration = aS.clip.length;
            aS.Play();
        }
        dialogueCounter = 0;
    }

    public void Play_GameplayResponse(GameplayResponseType responseType)
    {
        if (responseType == GameplayResponseType.UnitDeath)
        {
            SetSpeechState(SpeechState.Parallel);
            if (!aS.isPlaying)
            {
                PlayClip(GameplayResponses_UnitDeath[Random.Range(0, GameplayResponses_UnitDeath.Count - 1)]);
            }
        }
    }

    public void Play_Transition()
    {
        if (gm.numberOfRuns == 0)
        {
            SetSpeechState(SpeechState.Attention);
            PlayClip("intro_00");
            return;
        }
        if (gm.numberOfRuns > 0)
        {
            SetSpeechState(SpeechState.Attention);
            PlayClip("intro_01");
            return;
        }
    }
}
