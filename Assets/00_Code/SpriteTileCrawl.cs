﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteTileCrawl : MonoBehaviour
{
    private Renderer r;
    public Vector2 scrollSpeed = new Vector2(0,1);
    private Vector2 offSet;

    // Start is called before the first frame update
    void Start()
    {
        r = gameObject.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        offSet += scrollSpeed * Time.deltaTime;
        r.material.SetTextureOffset("_MainTex", offSet);
    }
}
