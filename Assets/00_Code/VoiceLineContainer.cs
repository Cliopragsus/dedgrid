﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceLineContainer : MonoBehaviour
{
    [Header("Opening")]
    public AudioClip intro_00;
    public AudioClip intro_00_Abort;
    public AudioClip intro_01;

    [Header("Gameplay Responses")]
    public AudioClip poeticEnd_00;
    public AudioClip tragic_00;
    public AudioClip greaterGood_00;

    [Header("Specific Reactions")]
    public AudioClip memory_Spear_00;
    public AudioClip memory_Spear_00_Abort;
}
