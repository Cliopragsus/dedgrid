﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public enum CameraState
    {
        Ingame,
        Menu
    }
    public CameraState currentCameraState;

    public GameObject eye;
    public GameObject objectOfInterest;
    public float rotationSpeed;
    public float moveSpeed;
    public float minCamHeight = 5;
    public float maxCamHeight = 10;
    public float camDist;
    public float scrollSensitivity = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (eye == null)
        {
            eye = GameObject.Find("Eye");
        }
    }

    // Update is called once per frame
    void Update()
    {
        Observe();
        Move();
    }

    public void Move()
    {
        camDist -= Input.mouseScrollDelta.y * scrollSensitivity;
        camDist = ClampValue(camDist);
        if (GameManager.instance.currentGameState == GameManager.GameState.Ingame)
        {
            eye.transform.position = new Vector3(objectOfInterest.transform.position.x + (objectOfInterest.transform.position.x - 4.5f) * 0.5f, CheckCameraHeight(objectOfInterest.transform.position.z), -2 + objectOfInterest.transform.position.z * 0.5f);

        }
        else if (GameManager.instance.currentGameState == GameManager.GameState.Paused)
        {
            eye.transform.position = new Vector3(4.5f, 2, -2);
        }
        transform.position = Vector3.MoveTowards(transform.position, eye.transform.position, moveSpeed * Time.deltaTime / Time.timeScale * Vector3.Distance(eye.transform.position, transform.position));
    }

    void Observe() // rotates the camera towards an eye object that always directly looks at the targeter with an upwards rotation
    {
        if (currentCameraState == CameraState.Ingame && objectOfInterest != null)
        {
            eye.transform.LookAt(objectOfInterest.transform.position);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, eye.transform.rotation, Quaternion.Angle(transform.rotation, eye.transform.rotation) * rotationSpeed);
        }
    }

    public void SetObjectofInterest(GameObject newObject) // sets the object that the camera will try to rotate towards
    {
        objectOfInterest = newObject;
    }

    public float CheckCameraHeight(float inputValue)
    {
        inputValue += camDist;
        if (inputValue < minCamHeight)
        {
            return minCamHeight;
        }
        if (inputValue > maxCamHeight)
        {
            return maxCamHeight;
        }
        return inputValue;
    }

    public float ClampValue(float v)
    {
        if (v < -5)
        {
            return -5;
        }
        if (v > 5)
        {
            return 5;
        }
        return v;
    }
}
