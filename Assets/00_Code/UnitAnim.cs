﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAnim : MonoBehaviour
{
    // Start is called before the first frame update
    private UnitBehaviour ub;
    private Animator anim;
    public bool isMoving = false;

    public void Start()
    {
        ub = gameObject.GetComponent<UnitBehaviour>();
        anim = gameObject.transform.GetComponentInChildren<Animator>();
    }

    public void OnAttack()
    {
        anim.Play("Attack");
    }

    public void OnDeath()
    {
        transform.Translate(0, -1.4f, 0);
        anim.enabled = false;
        gameObject.tag = "Corpse";
    }

    public void OnMove()
    {
        if (!isMoving)
        {
            isMoving = true;
            anim.Play("Walk");
        }
    }

    public void OnAbilityInitiation()
    {
        anim.Play("AbilityWindUp");
    }

    public void OnAbilityCast()
    {
        anim.Play("AbilityCast");
    }

    public void OnDestination()
    {
        isMoving = false;
        anim.Play("Idle");
    }
}
