﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabMaster : MonoBehaviour
{
    public static PrefabMaster instance;

    public GameObject coin;
    public GameObject contract;
    public GameObject contractIndicator;
    public GameObject[] terrains = new GameObject[3];
    public GameObject noUpgradeBlock;
    public GameObject selectionCircle;
    public GameObject hudCoin;
    public GameObject baseTerrain;
    public GameObject tree;

    [Header("Units")]
    public GameObject zombie_Base;
    public GameObject zombie_Spear;
    public GameObject zombie_Force;
    public GameObject knight_Base;
    public GameObject knight_Spear;
    public GameObject knight_Force;

    public void Start()
    {
        instance = this;
    }

    public GameObject NewTerrain()
    {
        return terrains[(int)Random.Range(0, terrains.Length - 1)];
    }
}
