﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public int x;
    public int z;
    // Start is called before the first frame update
    public void Register()
    {
        x = Mathf.RoundToInt(transform.position.x);
        z = Mathf.RoundToInt(transform.position.z);

        GameManager.instance.gp[x, z] = gameObject;
        GameManager.instance.gd[x, z] = "40000" + x.ToString() + z.ToString();
        //transform.parent = null;
        Debug.Log("Grid " + x + z + " is now registered as " + gameObject.name);
    }
}
