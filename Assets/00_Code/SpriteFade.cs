﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFade : MonoBehaviour
{
    public float fadeInSpeed = 1;
    public float fadeOutSpeed = 1;
    public float fadeInLimit = 1;
    public float fadeOutLimit = 0;
    public int goal = 1;
    public float alphaValue;
    private SpriteRenderer sr;
    public bool dieOnFade = false;

    public void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    public void Update()
    {
        sr.color = new Color(1, 1, 1, alphaValue);
        if (goal == 0 && alphaValue > fadeOutLimit)
        {
            alphaValue -= Time.deltaTime * fadeOutSpeed / Time.timeScale;
        }
        if (goal == 1 && alphaValue < fadeInLimit)
        {
            alphaValue += Time.deltaTime * fadeInSpeed / Time.timeScale;
        }
        if (dieOnFade)
        {
            if (alphaValue <= fadeOutLimit)
            {
                Destroy(gameObject);
            }
        }
    }

    public void FadeIn()
    {
        goal = 1;
    }

    public void FadeOut()
    {
        goal = 0;
    }
}
