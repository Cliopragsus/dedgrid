﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBehaviour_Force : UnitBehaviour
{
    private Vector3[] adjacentTiles = new Vector3[8];
    private Vector3 forceDirection;
    public float pushSpeed = 1;
    private float pushCount = 0;
    public float pushTime = 2;
    public Transform abilityEye;
    private RaycastHit[] scanResults = new RaycastHit[0];
    private List<GameObject> affectedUnits = new List<GameObject>();
    private List<GameObject> affectedClutter = new List<GameObject>();
    private List<GameObject> tempAffected = new List<GameObject>();
    private GameObject tempSort;
    private int tempIndex;
    private List<int> clearedIndeces = new List<int>();
    private Vector3[] affectedUnits_newPositions = new Vector3[0];
    private Vector3[] affectedClutter_newPositions = new Vector3[0];

    public override void AbilityAbortion()
    {
        Debug.Log("force AbilityAbortion");
        anim.OnDestination();
        abilityIndicator.transform.parent = null;
        Destroy(abilityIndicator);
        AbilityRoutine = null;
        ready_Ability = false;
        gm.desiredTimeScale = 1f;
    }

    public override bool AbilityCheck()
    {
        Debug.Log("force AbilityCheck");

        return true;
    }

    public override void AbilityExecution()
    {
        gm.desiredTimeScale = 1f;
        abilityIndicator.transform.parent = null;
        transform.LookAt(targeter.transform.position);
        anim.OnAbilityCast();
        Debug.Log("force AbilityExecution");
        GetAffectedObjects();
        foreach (GameObject unit in affectedUnits)
        {
            gm.gp[(int)unit.transform.position.x, (int)unit.transform.position.z] = null;
        }
        AbilityRoutine = MoveUnits;
    }

    public override void AbilityInitiation()
    {
        SetAdjacentTiles();
        Debug.Log("force AbilityInitiation");
        anim.OnAbilityInitiation();
        ready_Ability = true;
        targeter = gm.targeter;
        affectedUnits.Clear();
        AbilityRoutine = AbilityPreCastVisualization;
        abilityIndicator = Instantiate(abilityIndicator_Prefab, transform.position + Vector3.up * 0.1f, Quaternion.identity);
        abilityIndicator.transform.parent = transform;
    }

    public override void AbilityPreCastVisualization()
    {
        tX = (int)targeter.transform.position.x;
        tZ = (int)targeter.transform.position.z;
        DetermineForceDirection();
        abilityEye.LookAt(transform.position + forceDirection);
        abilityIndicator.transform.rotation = abilityEye.transform.rotation;
    }

    public void AbilityResolution()
    {
        Debug.Log("Force AbilityResolution");
        Destroy(abilityIndicator);
        for (int i = 0; i < affectedUnits.Count; i++)
        {
            affectedUnits[i].transform.position = affectedUnits_newPositions[i];
        }
        foreach (GameObject unit in affectedUnits)
        {
            unit.GetComponent<UnitBehaviour>().UpdatePositionInfo();
        }
        AbilityRoutine = null;
        ready_Ability = false;
        gm.RequestControlStateChange(GameManager.ControlState.Buffer);
    }

    public Vector3 CalculateNewPosition(int index)
    {
        Vector3 result = new Vector3(0,0,0);
        int x = (int)(affectedUnits[index].transform.position.x + forceDirection.x * 2);
        int z = (int)(affectedUnits[index].transform.position.z + forceDirection.z * 2);

        if (x >= 0 && x <= 9 && z >= 0 && z <= 9)
        {
            if (gm.gp[x - (int)forceDirection.x, z - (int)forceDirection.z] == null)
            {
                if (gm.gp[x, z] == null)
                {
                    gm.gp[x, z] = affectedUnits[index];
                    result = new Vector3(x, 0, z);
                    gm.gp[(int)affectedUnits[index].transform.position.x, (int)affectedUnits[index].transform.position.z] = null;
                    ClampVector(result);
                    return result;
                }
                else
                {
                    gm.gp[x - (int)forceDirection.x, z - (int)forceDirection.z] = affectedUnits[index];
                    result = new Vector3(x - (int)forceDirection.x, 0, z - (int)forceDirection.z);
                    gm.gp[(int)affectedUnits[index].transform.position.x, (int)affectedUnits[index].transform.position.z] = null;
                    ClampVector(result);
                    return result;
                }
            }
            else
            {
                result = affectedUnits[index].transform.position;
                return result;
            }
        }

        x = (int)(affectedUnits[index].transform.position.x + forceDirection.x * 1);
        z = (int)(affectedUnits[index].transform.position.z + forceDirection.z * 1);
        if (x >= 0 && x <= 9 && z >= 0 && z <= 9)
        {
            if (gm.gp[x, z] == null)
            {
                gm.gp[x, z] = affectedUnits[index];
                result = new Vector3(x, 0, z);
                gm.gp[(int)affectedUnits[index].transform.position.x, (int)affectedUnits[index].transform.position.z] = null;
                ClampVector(result);
                return result;
            }
            else
            {
                result = affectedUnits[index].transform.position;
                return result;
            }
        }

        result = ClampVector(affectedUnits[index].transform.position);
        return result;
    }

    public Vector3 ClampVector(Vector3 vector)
    {
        //Debug.Log("Clamp vector : " + vector);
        if (vector.x < 0)
        {
            vector = new Vector3(0, vector.y, vector.z);
        }
        if( vector.x > 9)
        {
            vector = new Vector3(9, vector.y, vector.z);
        }
        if (vector.z < 0)
        {
            vector = new Vector3(vector.x, vector.y, 0);
        }
        if (vector.z > 9)
        {
            vector = new Vector3(vector.x, vector.y, 9);
        }
        //Debug.Log("Clamp vector result : " + vector);
        return vector;
    }

    public void DetermineForceDirection()
    {
        forceDirection = new Vector3(99, 0, 99);
        for (int i = 0; i <= 6; i += 2)
        {
            if (Vector3.Distance(targeter.transform.position, adjacentTiles[i]) < Vector3.Distance(transform.position + forceDirection, targeter.transform.position))
            {
                forceDirection = (adjacentTiles[i] - transform.position).normalized;
            }
        }
        //Debug.Log(forceDirection);
    }

    public void GetAffectedObjects()
    {
        scanResults = Physics.BoxCastAll(transform.position, new Vector3(2, 2, 2),Vector3.down, Quaternion.identity, 0.1f);
        for(int i = 0; i < scanResults.Length; i++)
        {
            if (scanResults[i].collider.gameObject.tag == "Unit")
            {
                if (scanResults[i].collider.gameObject != gameObject)
                {
                    affectedUnits.Add(scanResults[i].collider.gameObject);
                }
            }
        }
        SortAffectedUnits();
    }

    public void MoveUnits()
    {
        for (int i = 0; i < affectedUnits.Count; i++)
        {
            affectedUnits[i].transform.position = Vector3.MoveTowards(affectedUnits[i].transform.position, affectedUnits_newPositions[i], pushSpeed * Time.deltaTime);
            if (Vector3.Distance(affectedUnits[i].transform.position, affectedUnits_newPositions[i]) < 0.1f)
            {
                affectedUnits[i].transform.position = affectedUnits_newPositions[i];
            }
        }
        pushCount += Time.deltaTime / Time.timeScale;
        if (pushCount >= pushTime)
        {
            pushCount = 0;
            AbilityResolution();
        }
    }

    public void SetAdjacentTiles()
    {
        adjacentTiles[0] = transform.position + Vector3.forward;
        adjacentTiles[1] = transform.position + Vector3.forward + Vector3.right;
        adjacentTiles[2] = transform.position + Vector3.right;
        adjacentTiles[3] = transform.position + Vector3.right + Vector3.back;
        adjacentTiles[4] = transform.position + Vector3.back;
        adjacentTiles[5] = transform.position + Vector3.back + Vector3.left;
        adjacentTiles[6] = transform.position + Vector3.left;
        adjacentTiles[7] = transform.position + Vector3.left + Vector3.forward;
    }

    public void SortAffectedUnits()
    {
        tempAffected.Clear();
        clearedIndeces.Clear();
        affectedUnits_newPositions = new Vector3[affectedUnits.Count];
        for (int a = 0; a < affectedUnits.Count; a++)
        {
            tempAffected.Add(affectedUnits[a]);
        }

        for (int i = 0; i < affectedUnits.Count; i++)
        {
            float dist = 99;
            for (int o = 0; o < tempAffected.Count; o++)
            {
                if (!clearedIndeces.Contains(o))
                {
                    if (Vector3.Distance(gameObject.transform.position + forceDirection * 4, affectedUnits[o].transform.position) < dist)
                    {
                        dist = Vector3.Distance(gameObject.transform.position + forceDirection * 4, affectedUnits[o].transform.position);
                        tempSort = affectedUnits[o];
                        tempIndex = o;
                    }
                }
            }
            clearedIndeces.Add(tempIndex);
            Debug.Log("Cleared Indeces");
            foreach(int x in clearedIndeces)
            {
                Debug.Log(x);
            }
            affectedUnits_newPositions[tempIndex] = CalculateNewPosition(tempIndex);
        }
    }
}
