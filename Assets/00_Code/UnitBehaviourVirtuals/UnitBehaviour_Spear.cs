﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBehaviour_Spear : UnitBehaviour
{

    Vector3 hookDirection = new Vector3(0, 0, 0);
    Vector3 hookSpot = new Vector3(0, 0, 0);
    int hookX = 0;
    int hookZ = 0;
    private Vector3[] adjacentTiles = new Vector3[8];
    private int adjacentTilesIndex = 0;
    private int adjacentTilesIndex_Upper = 0;
    private int adjacentTilesIndex_Lower = 0;
    private int distanceCount = 0;
    private GameObject hookSpotTile;
    public GameObject hookSpotTile_Prefab;
    private Vector3 hookTargetPos;
    private Vector3 old_hookTargetPos;
    public float pullSpeed = 2;
    private float pullCount;
    public float pullTime = 2;

    public override void AbilityAbortion()
    {
        Debug.Log("Spear AbilityAbortion");
        anim.OnDestination();
        abilityIndicator.transform.parent = null;
        Destroy(abilityIndicator);
        AbilityRoutine = null;
        ready_Ability = false;
        gm.desiredTimeScale = 1f;
    }

    public override bool AbilityCheck()
    {
        Debug.Log("Spear AbilityCheck");
        if (gm.gp[tX, tZ] != null)
        {
            if (gm.gp[tX, tZ].tag == "Unit")
            {
                if (gm.gp[tX, tZ].GetComponent<UnitBehaviour>().affiliation == 2)
                {
                    CalculateHookSpot();
                    if (hookSpot != new Vector3(99, 99, 99))
                    {
                        abilityTarget = gm.gp[tX, tZ];
                        return true;
                    }
                    else
                    {
                        abilityTarget = null;
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public override void AbilityExecution()
    {
        Debug.Log("Spear AbilityExecution");
        anim.OnAbilityCast();
        gm.desiredTimeScale = 1f;
        abilityTarget = gm.gp[tX, tZ];
        abilityIndicator.transform.parent = null;
        abilityIndicator.transform.parent = abilityTarget.transform;
        AbilityRoutine = null;
        hookSpotTile.GetComponent<SpriteFade>().FadeOut();
        hookSpotTile.GetComponent<SpriteFade>().dieOnFade = true;
        gm.gp[tX, tZ] = null;
        hookSpotTile = null;
        distanceCount = 1;
        AbilityRoutine = MoveUnit;
    }

    public override void AbilityInitiation()
    {
        SetAdjacentTiles();
        anim.OnAbilityInitiation();
        Debug.Log("Spear AbilityInitiation");
        ready_Ability = true;
        targeter = gm.targeter;
        AbilityRoutine = AbilityPreCastVisualization;
        abilityIndicator = Instantiate(abilityIndicator_Prefab, targeter.transform.position, Quaternion.identity);
        abilityIndicator.transform.parent = targeter.transform;
    }

    public override void AbilityPreCastVisualization()
    {
        tX = (int)targeter.transform.position.x;
        tZ = (int)targeter.transform.position.z;
        abilityTarget = gm.gp[tX, tZ];
        hookTargetPos = new Vector3(tX, 0, tZ);

        if (hookTargetPos != old_hookTargetPos)
        {
            old_hookTargetPos = hookTargetPos;
            if (AbilityCheck())
            {
                abilityIndicator.transform.GetChild(0).GetComponent<MeshRenderer>().material = legalMat;
                if (hookSpotTile != null)
                {
                    hookSpotTile.GetComponent<SpriteFade>().FadeOut();
                    hookSpotTile.GetComponent<SpriteFade>().dieOnFade = true;
                    hookSpotTile = null;
                }
                hookSpotTile = Instantiate(hookSpotTile_Prefab, hookSpot + Vector3.up * 0.1f, Quaternion.identity);
                hookSpotTile.GetComponent<SpriteFade>().FadeIn();
                hookSpotTile.transform.Rotate(90, 0, 0);
                // calculate field that either the caster or the target are being pulled towards
            }
            else
            {
                abilityIndicator.transform.GetChild(0).GetComponent<MeshRenderer>().material = illegalMat;
                if (hookSpotTile != null)
                {
                    hookSpotTile.GetComponent<SpriteFade>().FadeOut();
                    hookSpotTile.GetComponent<SpriteFade>().dieOnFade = true;
                    hookSpotTile = null;
                }
                // turn ability indicator red and low alpha
            }
        }
        //Debug.Log("Spear AbilityPreCastVisualization");
        if (AbilityCheck())
        {
            Vector3[] line = new Vector3[2];

            line[0] = hookSpot;
            line[1] = abilityIndicator.transform.position;
            LineRenderer lr = GetComponent<LineRenderer>();
            lr.positionCount = 2;
            lr.SetPositions(line);
        }
    }

    public void AbilityResolution()
    {
        Debug.Log("Spear AbilityResolution");
        AbilityRoutine = null;
        ready_Ability = false;
        abilityTarget = null;
        GetComponent<LineRenderer>().SetPositions(new Vector3[0]);
        Destroy(abilityIndicator);
        gm.RequestControlStateChange(GameManager.ControlState.Buffer);
    }

    public void CalculateHookSpot()
    {
        distanceCount = 1;
        hookDirection = (new Vector3(tX, 0, tZ) - transform.position).normalized;
        Debug.Log(hookDirection);
        hookX = Mathf.RoundToInt(transform.position.x) + Mathf.RoundToInt(hookDirection.x);
        hookZ = Mathf.RoundToInt(transform.position.z) + Mathf.RoundToInt(hookDirection.z);
        for (int i = 0; i < adjacentTiles.Length; i++)
        {
            if (adjacentTiles[i] == new Vector3(hookX, 0, hookZ))
            {
                adjacentTilesIndex = i;
                adjacentTilesIndex_Upper = i + 1;
                adjacentTilesIndex_Lower = i - 1;
                if (adjacentTilesIndex_Upper == 8)
                {
                    adjacentTilesIndex_Upper = 0;
                }
                if (adjacentTilesIndex_Lower == -1)
                {
                    adjacentTilesIndex_Lower = 7;
                }
            }
        }
        if (gm.gp[hookX, hookZ] == null)
        {
            hookSpot = new Vector3(hookX, 0, hookZ);
            Debug.Log("Found HookSpot at " + hookSpot);
            return;
        }
        else
        {
            for (int i = 0; i < Vector3.Distance(transform.position, new Vector3(tX, 0, tZ)) + 1; i++)
            {
                if (gm.gp[(int)adjacentTiles[adjacentTilesIndex].x + Mathf.RoundToInt(hookDirection.x) * distanceCount, (int)adjacentTiles[adjacentTilesIndex].z + Mathf.RoundToInt(hookDirection.z) * distanceCount] == null)
                {
                    hookSpot = adjacentTiles[adjacentTilesIndex] + new Vector3(Mathf.RoundToInt(hookDirection.x) * distanceCount, 0, Mathf.RoundToInt(hookDirection.z) * distanceCount);
                    return;
                }
                else if (Vector3.Distance(adjacentTiles[adjacentTilesIndex_Upper], new Vector3(tX, 0, tZ)) < Vector3.Distance(adjacentTiles[adjacentTilesIndex_Lower], new Vector3(tX, 0, tZ)))
                {
                    if (gm.gp[(int)adjacentTiles[adjacentTilesIndex_Upper].x + Mathf.RoundToInt(hookDirection.x) * distanceCount, (int)adjacentTiles[adjacentTilesIndex_Upper].z + Mathf.RoundToInt(hookDirection.z) * distanceCount] == null)
                    {
                        hookSpot = adjacentTiles[adjacentTilesIndex_Upper] + new Vector3(Mathf.RoundToInt(hookDirection.x) * distanceCount, 0, Mathf.RoundToInt(hookDirection.z) * distanceCount);
                        return;
                    }
                    else if (gm.gp[(int)adjacentTiles[adjacentTilesIndex_Lower].x + Mathf.RoundToInt(hookDirection.x) * distanceCount, (int)adjacentTiles[adjacentTilesIndex_Lower].z + Mathf.RoundToInt(hookDirection.z) * distanceCount] == null)
                    {
                        hookSpot = adjacentTiles[adjacentTilesIndex_Lower] + new Vector3(Mathf.RoundToInt(hookDirection.x) * distanceCount, 0, Mathf.RoundToInt(hookDirection.z) * distanceCount);
                        return;
                    }
                }
                else
                {
                    if (gm.gp[(int)adjacentTiles[adjacentTilesIndex_Lower].x + Mathf.RoundToInt(hookDirection.x) * distanceCount, (int)adjacentTiles[adjacentTilesIndex_Lower].z + Mathf.RoundToInt(hookDirection.z) * distanceCount] == null)
                    {
                        hookSpot = adjacentTiles[adjacentTilesIndex_Lower] + new Vector3(Mathf.RoundToInt(hookDirection.x) * distanceCount, 0, Mathf.RoundToInt(hookDirection.z) * distanceCount);
                        return;
                    }
                    else if (gm.gp[(int)adjacentTiles[adjacentTilesIndex_Upper].x + Mathf.RoundToInt(hookDirection.x) * distanceCount, (int)adjacentTiles[adjacentTilesIndex_Upper].z + Mathf.RoundToInt(hookDirection.z) * distanceCount] == null)
                    {
                        hookSpot = adjacentTiles[adjacentTilesIndex_Upper] + new Vector3(Mathf.RoundToInt(hookDirection.x) * distanceCount, 0, Mathf.RoundToInt(hookDirection.z) * distanceCount);
                        return;
                    }
                }
                distanceCount++;
            }
        }
        hookSpot = new Vector3(99, 99, 99);
        Debug.Log("Spear: No free tile to hook onto found");
        
        //hookSpots[0] = transform.position;
    }

    public void MoveUnit()
    {
        Debug.Log("Spear MoveUnit");
        abilityTarget.transform.position = Vector3.MoveTowards(abilityTarget.transform.position, hookSpot, pullSpeed * Time.deltaTime);
        if (Vector3.Distance(abilityTarget.transform.position, hookSpot) < 0.1f)
        {
            abilityTarget.transform.position = hookSpot;
            gm.gp[(int)abilityTarget.transform.position.x, (int)abilityTarget.transform.position.z] = abilityTarget;
        }

        pullCount += Time.deltaTime / Time.timeScale;
        if (pullCount >= pullTime)
        {
            pullCount = 0;
            AbilityResolution();
        }
    }

    public void SetAdjacentTiles()
    {
        adjacentTiles[0] = transform.position + Vector3.forward;
        adjacentTiles[1] = transform.position + Vector3.forward + Vector3.right;
        adjacentTiles[2] = transform.position + Vector3.right;
        adjacentTiles[3] = transform.position + Vector3.right + Vector3.back;
        adjacentTiles[4] = transform.position + Vector3.back;
        adjacentTiles[5] = transform.position + Vector3.back + Vector3.left;
        adjacentTiles[6] = transform.position + Vector3.left;
        adjacentTiles[7] = transform.position + Vector3.left + Vector3.forward;
    }
}
