﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBehaviour : MonoBehaviour
{
    [Header("General")]
    public int affiliation; // 0 = allies | 2 = enemies
    public string unitType = "Base";
    public int healthPoints = 1;
    public int resurrectionCost = 1;

    public int posX;
    public int posZ;

    public GameObject target;
    public Vector3 targetPos;
    public delegate void Exe();
    public Exe Execute;
    public bool waitingForNextInstruction = true;
    public float attackCounter;
    public GameObject targeter;

    [Header("Ability Specifics")]
    public int ability_Cost = 0;
    public bool ready_Ability = false;
    public delegate void abilityRoutine();
    public abilityRoutine AbilityRoutine;
    public GameObject abilityTarget;
    public int tX; // targeter x coordinate
    public int tZ; // targeter z coordinate
    public GameObject abilityIndicator_Prefab;
    [HideInInspector] public GameObject abilityIndicator;
    public Material legalMat;
    public Material illegalMat;


    [HideInInspector] public UnitAnim anim;
    [HideInInspector] public Pathing p;
    [HideInInspector] public GameManager gm;

    // Start is called before the first frame update
    public void Start()
    {
        p = GetComponent<Pathing>();
        anim = gameObject.GetComponent<UnitAnim>();
        gm = GameManager.instance;
        posX = Mathf.RoundToInt(transform.position.x);
        posZ = Mathf.RoundToInt(transform.position.z);
        //transform.position = new Vector3(posX, 0, posZ);
        //Register();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.currentGameState == GameManager.GameState.Ingame)
        {
            if (GameManager.instance.currentControlState == GameManager.ControlState.Buffer)
            {
                CorrectView();
            }
            if (GameManager.instance.currentControlState == GameManager.ControlState.Execution && !waitingForNextInstruction)
            {
                Execute();
            }

            if (ready_Ability)
            {
                AbilityRoutine();
            }
        }
    }

    public virtual void AbilityAbortion()
    {
        Debug.Log("Base AbilityAbortion");
        ready_Ability = false;
    }

    public virtual bool AbilityCheck()
    {
        Debug.Log("Base Ability Check");
        return false;
    }

    public virtual void AbilityExecution()
    {
        Debug.Log("Base AbilityExecution");
    }

    public virtual void AbilityInitiation()
    {
        targeter = gm.targeter;
        Debug.Log("Base AbilityInitiation");
        ready_Ability = true;
        AbilityRoutine = AbilityPreCastVisualization;
        //anim.OnCast();
    }

    public virtual void AbilityPreCastVisualization()
    {
        Debug.Log("Base Class Virtual Ability visualized");
    }

    public void Act()
    {
        Vector3 nextPos = p.RequestMove(affiliation);
        int npX = (int)nextPos.x;
        int npZ = (int)nextPos.z;
        if (target == null)
        {
            Vector3 scanPos = p.ScanForHostiles(affiliation);
            if (scanPos != new Vector3(99, 99, 99))
            {
                target = gm.gp[(int)scanPos.x, (int)scanPos.z];
                targetPos = scanPos;
                AttackOrder((int)scanPos.x, (int)scanPos.z);
            }
            else
            {
                MoveOrder(npX, npZ);
            }
        }
        else if (nextPos == target.transform.position)
        {
            if (target.GetComponent<Coin>())
            {
                MoveOrder(npX, npZ);
            }
            if (target.tag == "Contract")
            {
                MoveOrder(npX, npZ);
            }
            if (target.GetComponent<UnitBehaviour>())
            {
                AttackOrder(npX, npZ);
            }
        }
        else
        {
            MoveOrder(npX, npZ);
        }
    }

    public void Attack()
    {
        if (waitingForNextInstruction == false)
        {
            anim.OnAttack();
            if (attackCounter <= 2)
            {
                attackCounter += Time.deltaTime;
            }
            else
            {
                if (gm.gp[(int)targetPos.x, (int)targetPos.z] != null)
                {
                    gm.gp[(int)targetPos.x, (int)targetPos.z].GetComponent<UnitBehaviour>().TakeDamage();
                }
                target = null;
                waitingForNextInstruction = true;
                attackCounter = 0;
                GameManager.instance.Feedback();
                Debug.Log("Attack Command happened");
            }
        }
    }

    public void AttackOrder(int x, int z)
    {
        targetPos = new Vector3(x, 0, z);
        waitingForNextInstruction = false;
        transform.LookAt(targetPos);
        Execute = Attack;
    }

    public void CorrectView()
    {
        if (waitingForNextInstruction == false)
        {
            transform.LookAt(targetPos);
            GameManager.instance.Feedback();
            //waitingForNextInstruction = true;
        }
    }


    public void Move()
    {
        if (waitingForNextInstruction == false)
        {
            transform.LookAt(targetPos);
            transform.position = Vector3.MoveTowards(transform.position, targetPos, 1f * Time.deltaTime);
            if (Vector3.Distance(targetPos, transform.position) < 0.2f)
            {
                if (affiliation == 2)
                {
                    if (transform.position.z < 1)
                    {
                        GameManager.instance.Feedback();
                        TakeDamage();
                    }
                }
                transform.position = targetPos;
                Debug.Log(gameObject.name + " is giving feedback to gm");
                UpdatePositionInfo();
                GameManager.instance.Feedback();
                anim.OnDestination();
                waitingForNextInstruction = true;
                if (target != null)
                {
                    p.RequestNewPath(affiliation + 1);
                }
            }
        }
    }

    public void MoveOrder(int x, int z)
    {
        targetPos = new Vector3(x, 0, z);
        gm.SetMoveProxy(x, z);
        gm.gp[(int)transform.position.x, (int)transform.position.z] = null;
        waitingForNextInstruction = false;
        Execute = Move;
        anim.OnMove();
    }

    public void Register()
    {
        posX = Mathf.RoundToInt(transform.position.x);
        posZ = Mathf.RoundToInt(transform.position.z);
        transform.position = new Vector3(posX, 0, posZ);
        //transform.parent = null;
        GameManager.instance.gp[posX, posZ] = gameObject;
        if (affiliation == 0)
        {
            GameManager.instance.allies.Add(gameObject);
        }
        if (affiliation == 2)
        {
            GameManager.instance.enemies.Add(gameObject);
        }
        Debug.Log("Grid " + posX + posZ + " is now registered as " + gameObject.name);
    }

    public void TakeDamage()
    {
        healthPoints = 0;
        if (affiliation == 0)
        {
            gm.allies.Remove(gameObject);
        }
        else if (affiliation == 2)
        {
            gm.enemies.Remove(gameObject);
        }
        gm.tde.Play_GameplayResponse(TDE.GameplayResponseType.UnitDeath);
        gm.gp[(int)transform.position.x, (int)transform.position.z] = null;
        gameObject.tag = "Corpse";
        GetComponent<LineRenderer>().enabled = false;
        anim.OnDeath();
        transform.parent = gm.oldSegment.transform;
    }

    public void UpdatePositionInfo() // deletes old grid information after leaving the field, then updates its own and the grid info
    {
        //GameManager.instance.gp[posX, posZ] = null;
        posX = (int)transform.position.x;
        posZ = (int)transform.position.z;
        GameManager.instance.gp[posX, posZ] = gameObject;
    }
}
