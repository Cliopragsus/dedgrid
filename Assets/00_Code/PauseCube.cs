﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseCube : MonoBehaviour
{
    public Quaternion currentRotation;
    public Quaternion[] rotations = new Quaternion[4];
    public int rotationIndex = 0;
    public Quaternion targetRotation;
    public float rotationSpeed = 1;

    void Update()
    {
        currentRotation = transform.rotation;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime / Time.timeScale);//Quaternion.Angle(transform.rotation, targetRotation) * Time.deltaTime * rotationSpeed);
    }

    public void NextMenu(bool turnLeft)
    {
        if (turnLeft)
        {
            rotationIndex = SkipInt(rotationIndex - 1);
            targetRotation = rotations[rotationIndex];
        }
        else
        {
            rotationIndex = SkipInt(rotationIndex + 1);
            targetRotation = rotations[rotationIndex];
        }
    }

    public int SkipInt(int index)
    {
        if (index < 0)
        {
            return 3;
        }
        if (index > 3)
        {
            return 0;
        }
        return index;
    }
}
