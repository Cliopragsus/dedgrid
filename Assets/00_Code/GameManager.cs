﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class GameManager : MonoBehaviour
{
    public enum GameState
    {
        Init,
        Ingame,
        Transition,
        Paused,
        Shutdown
    }
    public GameState currentGameState;
    public GameState lastGameState;

    public enum ControlState
    {
        Inputphase,
        Buffer,
        Execution,
        EnemyTurn,
        Dialogue
    }
    public ControlState currentControlState;
    public ControlState lastControlState;
    public enum RushState
    {
        Inputphase,
        Animation
    }
    public RushState currentRushState;
    public bool runActive;

    public static GameManager instance;

    [Header("Control")]
    public int gridLengthX = 10;
    public int gridLengthY = 10;
    public GameObject[,] gp = new GameObject[0, 0]; // holds the gameobject units                                             (grid physical)
    public string[,] gd = new string[0, 0]; // stores information in a 7-digit number as a string, used for navigation etc    (grid  digits)
    /// <summary>
    /// 1st - 0 = empty / 1 = playerunit / 2 = enemyunit / 3 = item / 4 = environment / 5 = walkqueue destination / 6 = Coin
    /// 2nd - type of unit
    /// 3rd - healthpoints
    /// 4th - abilitycooldown
    /// 5th - facingdirection; 0 = north / 1 = east / 2 = south / 3 = west
    /// 6th - x coordinate
    /// 7th - z coordinate
    /// </summary>
    public List<GameObject> allies = new List<GameObject>();
    public List<GameObject> enemies = new List<GameObject>();
    public int feedbackCount = 0;
    public GameObject targeter;
    public GameObject currentSelection;
    public GameObject tempObject;
    public float desiredTimeScale = 1;
    public float currentTimeScale = 1;
    public float timeScaleGrowth = 1;
    private GameObject selectionCircle;

    [Header("Abilities")]
    public bool ready_Coin = false;
    public bool ready_Contract = false;
    public bool ready_Ability = false;
    public bool ready_Resurrection = false;
    public bool ready_Resurrection_Advanced = false;

    public delegate void ActionSequence();
    public ActionSequence ActionQueue_Zero;
    public ActionSequence ActionQueue_One;
    public ActionSequence ActionQueue_Two;
    public ActionSequence actionSequenceHolder;
    public int actionQueueIndex = 0;

    private RaycastHit[] resurrectionRayCastHits = new RaycastHit[0];
    public List<GameObject> resurrectionTargets = new List<GameObject>();
    public List<GameObject> resurrectionShowReel = new List<GameObject>();
    public List<GameObject> resurrectionShowReel_Advanced = new List<GameObject>();
    public Vector3[] resurrectionShowReelPositions = new Vector3[5];
    public Vector3[] resurrectionShowReelPositions_Advanced = new Vector3[5];
    public int resurrectionShowReelIndex = 0;
    public int resurrectionShowReelIndex_Advanced = 0;

    [Header("Currency")]
    public int money = 0;
    public List<int> floatingCost = new List<int>();
    public int deptIndex = 0;
    public GameObject coinHolder;
    [HideInInspector] public Animator coinAnim;
    public Text moneyText;

    [Header("LevelSequencing")]
    public float transitionSpeed = 2;
    public float newSegmentSpeed = 2;
    public int levelIndex;
    public bool nextSegmentinstantiated = false;
    public bool newTerrainElevated = false;
    public List<GameObject> transitionCloud_go = new List<GameObject>();
    public List<Vector3> transitionCloud_v3 = new List<Vector3>();
    public Vector3 newTerrainSpawnLocation;
    public Vector3 newTerrainPosition;
    public Vector3 terrainMainPosition;
    public Vector3 oldTerrainPosition;
    public Vector3 trashTerrainPosition;
    public GameObject newSegment;
    public GameObject oldSegment;
    public GameObject trashSegment;
    private Vector3[] newAllyPositions = new Vector3[0];
    public Vector3[] transitionSpots = new Vector3[3];

    [Header("Dialogue")]
    [HideInInspector] public TDE tde;
    public float gameplayResponseChance = 0.3f;
    

    [Header("Camera")]
    public GameObject cameraObject;
    public float viewSensitivity;
    [HideInInspector] public CameraBehaviour cb;
    private float mouseXdif = 0;
    private float mouseYdif = 0;

    [Header("Other")]
    public GameObject winText;
    public GameObject pauseCube;
    public GameObject currentCoin;
    public GameObject moveProxy;
    public GameObject grid;
    private SpriteFade gridSF;
    private bool gridToggleStay = false;
    private float gridToggleCounter = 0;

    [Header("Saving")]
    string json;
    [HideInInspector] public int numberOfRuns;

    [Header("Debug")]
    public string turn;
    public GameObject debugInterface;
    public Text turnText;
    public Text controlStateText;
    public GameObject gridInterface;
    public float pathingIntervalCounter = 0f;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        gp = new GameObject[gridLengthX, gridLengthY];
        gd = new string[gridLengthX, gridLengthY];
        gridSF = grid.GetComponent<SpriteFade>();
        Payment_UpdateMoneyText();
        tde = GetComponent<TDE>();
        //EmptyGrid();
        //DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        //SaveState_Load();
        RequestGameStateChange(GameState.Init);
        ActionQueue_Zero = DecoyVoid;
        ActionQueue_One = DecoyVoid;
        ActionQueue_Two = DecoyVoid;
        //GameInit();
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        GameInit();
        Ingame();
        Transition();

        Pause();

        Debugging();
        AdjustMouseSensitivity();
        Cheats();
        TimeScaleModify();
    }

    void Action() // function that uses the targeters position to ask the gp for information and offer corresponding actions
    {
        ///////////////////////////////// M1 Down ///////////////////////////////////

        if (Input.GetMouseButtonDown(0))
        {
            int x = Mathf.CeilToInt(targeter.transform.position.x);
            int z = Mathf.CeilToInt(targeter.transform.position.z);

            /// --- Ability Check --- ///
            /// 
            if (ready_Ability)
            {
                UnitBehaviour ub = currentSelection.GetComponent<UnitBehaviour>();
                if (ub.AbilityCheck())
                {
                    ub.AbilityExecution();
                    ready_Ability = false;
                }
            }

                if (isAlly(x,z) && NoActionInput())
            {
                currentSelection = gp[x, z];
                if (money >= currentSelection.GetComponent<UnitBehaviour>().ability_Cost)
                {
                    desiredTimeScale = 0.25f;
                    Payment_Initiation(currentSelection.GetComponent<UnitBehaviour>().ability_Cost);
                    currentSelection.GetComponent<UnitBehaviour>().AbilityInitiation();
                    ready_Ability = true;
                }
                else
                {
                    //blink red + error sound
                }
            }
            if (ready_Resurrection)
            {
                ready_Resurrection_Advanced = true;
                ResurrectionTargets_Show_Advanced();
            }
        }

        ///////////////////////////////////// M1 Get /////////////////////////////////

        if (Input.GetMouseButton(0))
        {
            if (currentGameState == GameState.Paused)
            {
                pauseCube.GetComponent<PauseCube>().NextMenu(true);
            }
            /*
            int x = Mathf.CeilToInt(targeter.transform.position.x);
            int z = Mathf.CeilToInt(targeter.transform.position.z);

            /// --- Contract --- ///
            
            if (currentSelection != null && ready_Contract == true)
            {
                pathingIntervalCounter += Time.deltaTime;
                if (pathingIntervalCounter >= 0.2f)
                {
                    Debug.Log("PA_Contract : requesting pathdata for " + currentSelection.name + " towards " + tempObject.transform.position);
                    currentSelection.GetComponent<UnitBehaviour>().target = tempObject;
                    currentSelection.GetComponent<Pathing>().RequestNewPath(1);
                    pathingIntervalCounter = 0;
                    return;
                }
            }
            */
        }

        ////////////////////////////////////// M1 Up //////////////////////////////////
        ///
        if (Input.GetMouseButtonUp(0))
        {
            int x = Mathf.CeilToInt(targeter.transform.position.x);
            int z = Mathf.CeilToInt(targeter.transform.position.z);


            if (ready_Ability)
            {
                UnitBehaviour ub = currentSelection.GetComponent<UnitBehaviour>();
                if (ub.AbilityCheck())
                {
                    ub.AbilityExecution();
                    ready_Ability = false;
                }
                /*
                else
                {
                    ub.AbilityAbortion();
                    ready_Ability = false;
                }
                */
            }

            if (ready_Resurrection_Advanced)
            {
                if (resurrectionShowReel_Advanced[resurrectionShowReelIndex_Advanced].GetComponent<UnitBehaviour>())
                {
                    GameObject newUnit = Instantiate(ResurrectionSpawnCorresponding(resurrectionShowReel_Advanced[resurrectionShowReelIndex_Advanced]), targeter.transform.position, Quaternion.identity);
                    newUnit.GetComponent<UnitBehaviour>().Start();
                    newUnit.GetComponent<UnitBehaviour>().Register();
                    newUnit.GetComponent<Pathing>().Start();
                    newUnit.GetComponent<UnitAnim>().Start();
                    newUnit.GetComponentInChildren<Shader_FadeIn>().alphaValue = 0f;
                    ResurrectionTargets_Remove_Advanced();
                    ResurrectionTargets_Remove();
                    desiredTimeScale = 1;
                    RequestControlStateChange(ControlState.Buffer);
                }
                else
                {
                    if (ready_Resurrection)
                    {
                        ResurrectionTargets_Remove_Advanced();
                        desiredTimeScale = 1;
                    }
                    else
                    {
                        ResurrectionTargets_Remove();
                        ResurrectionTargets_Remove_Advanced();
                        desiredTimeScale = 1;
                    }
                }
            }

            if (currentSelection != null && ready_Contract)
            {
                /*
                /// --- Contract --- ///
                if (gp[x, z] == null)
                {
                    Debug.Log("PA_Contract : placed Contract for " + currentSelection.name + " at " + x + z);
                    Destroy(tempObject);
                    tempObject = Instantiate(PrefabMaster.instance.contract, targeter.transform.position, Quaternion.identity);
                    tempObject.GetComponent<Contract>().SetUpContract(currentSelection);
                    tempObject = null;
                    ready_Contract = false;
                    currentSelection = null;
                    RequestControlStateChange(ControlState.Buffer);
                    return;
                }
                else
                {
                    Debug.Log("PA_Contract : No legal position to place the contract was found");
                    Destroy(tempObject);
                    tempObject = null;
                    ready_Contract = false;
                    return;
                }
                */
            }

            if (currentSelection == null && ready_Coin)
            {
                /// --- Coin --- ///
                if (gp[x, z] == null)
                {
                    ready_Coin = false;
                    SpawnCoin(x, z);
                    RequestControlStateChange(ControlState.Buffer);
                    Debug.Log("PA_PlaceCoin : Placing coin at " + x + z);
                    return;
                }
                else
                {
                    ready_Coin = false;
                    Debug.Log("PA_PlaceCoin : Target location was not null. Coin was not placed");
                    return;
                }
            }
        }

        ///////////////////////////////   M2 Down  /////////////////////////////

        if (Input.GetMouseButtonDown(1))
        {
            if (ready_Ability)
            {
                UnitBehaviour ub = currentSelection.GetComponent<UnitBehaviour>();
                ub.AbilityAbortion();
                Payment_Abortion();
                ready_Ability = false;
            }

            if (ready_Resurrection_Advanced)
            {
                ResurrectionTargets_Remove_Advanced();
            }

            if (currentGameState == GameState.Paused)
            {
                pauseCube.GetComponent<PauseCube>().NextMenu(false);
            }
        }

        /////////////////////////////// Space Down /////////////////////////////

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (actionQueueIndex <= 1 && NoActionInput())
            {
                //ActionQueue(Action_Pass);
                //Action_Pass();
                //actionQueueIndex++;

                // make unit pass
                int x = Mathf.CeilToInt(targeter.transform.position.x);
                int z = Mathf.CeilToInt(targeter.transform.position.z);
                if (gp[x, z] != null && NoActionInput())
                {
                    if (x >= 0 && x <= 9 && z >= 0 && z + 1 <= 9)
                    {
                        currentSelection = gp[x, z];
                        if (currentSelection.tag == "Unit")
                        {
                            if (currentSelection.GetComponent<UnitBehaviour>().affiliation == 0)
                            {
                                if (gp[x, z + 1] == null)
                                {
                                    if (actionQueueIndex <= 1)
                                    {
                                        Action_QuickMove_W();
                                        //ActionQueue(Action_QuickMove_W);
                                        //actionQueueIndex++;
                                    }
                                }
                                else if (gp[x, z + 1].tag != "Obstacle" && gp[x, z + 1].tag != "Move Proxy" && gp[x, z + 1].tag != "Unit")
                                {
                                    if (actionQueueIndex <= 1)
                                    {
                                        Action_QuickMove_Space();
                                        //ActionQueue(Action_QuickMove_W);
                                        //actionQueueIndex++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        ///////////////////////////////     H      ////////////////////////////
        if (Input.GetKeyDown(KeyCode.H))
        {
            Debug_PrintGridToConsole();
        }

        //////////////////////////////      R      ////////////////////////////
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (NoActionInput())
            {
                ready_Resurrection = true;
                //desiredTimeScale = 0.02f;
                ResurrectionTargets_Show();
            }
        }
        if (Input.GetKeyUp(KeyCode.R))
        {
            if (ready_Resurrection && !ready_Resurrection_Advanced)
            {
                ready_Resurrection = false;
                //desiredTimeScale = 1;
                ResurrectionTargets_Remove();
            }
        }
        //////////////////////////////  Quick Move  ///////////////////////////
        //////////////////////////////      W      ////////////////////////////
        if (Input.GetKeyDown(KeyCode.W))
        {
            int x = Mathf.CeilToInt(targeter.transform.position.x);
            int z = Mathf.CeilToInt(targeter.transform.position.z);
            if (gp[x, z] != null && NoActionInput())
            {
                if (x >= 0 && x <= 9 && z >= 0 && z + 1 <= 9)
                {
                    currentSelection = gp[x, z];
                    if (currentSelection.tag == "Unit")
                    {
                        if (currentSelection.GetComponent<UnitBehaviour>().affiliation == 0)
                        {
                            if (gp[x, z + 1] == null)
                            {
                                if (actionQueueIndex <= 1)
                                {
                                    Action_QuickMove_W();
                                    //ActionQueue(Action_QuickMove_W);
                                    //actionQueueIndex++;
                                }
                            }
                            else if (gp[x, z + 1].tag != "Obstacle" && gp[x, z + 1].tag != "Move Proxy" && gp[x, z + 1].tag != "Unit")
                            {
                                if (actionQueueIndex <= 1)
                                {
                                    Action_QuickMove_W();
                                    //ActionQueue(Action_QuickMove_W);
                                    //actionQueueIndex++;
                                }
                            }
                        }
                    }
                }
            }
        }

        //////////////////////////////      A      /////////////////////////////
        if (Input.GetKeyDown(KeyCode.A))
        {
            int x = Mathf.CeilToInt(targeter.transform.position.x);
            int z = Mathf.CeilToInt(targeter.transform.position.z);
            if (gp[x, z] != null && NoActionInput())
            {
                if (x - 1 >= 0 && x <= 9 && z >= 0 && z <= 9)
                {
                    currentSelection = gp[x, z];
                    if (currentSelection.tag == "Unit")
                    {
                        if (currentSelection.GetComponent<UnitBehaviour>().affiliation == 0)
                        {
                            if (gp[x - 1, z] == null)
                            {
                                if (actionQueueIndex <= 1)
                                {
                                    Action_QuickMove_A();
                                    //ActionQueue(Action_QuickMove_A);
                                    //actionQueueIndex++;
                                }
                            }
                            else if (gp[x - 1, z].tag != "Obstacle" && gp[x - 1, z].tag != "Move Proxy" && gp[x - 1, z].tag != "Unit")
                            {
                                if (actionQueueIndex <= 1)
                                {
                                    Action_QuickMove_A();
                                    //ActionQueue(Action_QuickMove_A);
                                    //actionQueueIndex++;
                                }
                            }
                        }
                    }
                } 
            }
        }

        //////////////////////////////      S      /////////////////////////////
        if (Input.GetKeyDown(KeyCode.S))
        {
            int x = Mathf.CeilToInt(targeter.transform.position.x);
            int z = Mathf.CeilToInt(targeter.transform.position.z);
            if (gp[x, z] != null && NoActionInput())
            {
                if (x >= 0 && x <= 9 && z - 1 >= 0 && z <= 9)
                {
                    currentSelection = gp[x, z];
                    if (currentSelection.tag == "Unit")
                    {
                        if (currentSelection.GetComponent<UnitBehaviour>().affiliation == 0)
                        {
                            if (gp[x, z - 1] == null)
                            {
                                if (actionQueueIndex <= 1)
                                {
                                    Action_QuickMove_S();
                                    //ActionQueue(Action_QuickMove_S);
                                    //actionQueueIndex++;
                                }
                            }
                            else if (gp[x, z - 1].tag != "Obstacle" && gp[x, z - 1].tag != "Move Proxy" && gp[x, z - 1].tag != "Unit")
                            {
                                if (actionQueueIndex <= 1)
                                {
                                    Action_QuickMove_S();
                                    //ActionQueue(Action_QuickMove_S);
                                    //actionQueueIndex++;
                                }
                            }
                        }
                    }
                }
            }
        }

        //////////////////////////////      D      /////////////////////////////
        if (Input.GetKeyDown(KeyCode.D))
        {
            int x = Mathf.CeilToInt(targeter.transform.position.x);
            int z = Mathf.CeilToInt(targeter.transform.position.z);
            if (gp[x, z] != null && NoActionInput())
            {
                if (x >= 0 && x  + 1 <= 9 && z >= 0 && z <= 9)
                {
                    currentSelection = gp[x, z];
                    if (currentSelection.tag == "Unit")
                    {
                        if (currentSelection.GetComponent<UnitBehaviour>().affiliation == 0)
                        {
                            if (gp[x + 1, z] == null)
                            {
                                if (actionQueueIndex <= 1)
                                {
                                    Action_QuickMove_D();
                                    //ActionQueue(Action_QuickMove_D);
                                    //actionQueueIndex++;
                                }
                            }
                            else if (gp[x + 1, z].tag != "Obstacle" && gp[x + 1, z].tag != "Move Proxy" && gp[x + 1, z].tag != "Unit")
                            {
                                if (actionQueueIndex <= 1)
                                {
                                    Action_QuickMove_D();
                                    //ActionQueue(Action_QuickMove_D);
                                    //actionQueueIndex++;
                                }
                            }
                        }
                    }
                }
            }
        }

        ///////////////////////// Numbers ////////////////////////////
        Action_SelectUnit();
    }

    public void ActionQueue(ActionSequence action)
    {
        if (actionQueueIndex == 0)
        {
            ActionQueue_Zero = action;
        }
        else if (actionQueueIndex == 1)
        {
            ActionQueue_One = action;
        }
        else if (actionQueueIndex == 2)
        {
            ActionQueue_Two = action;
        }
        else
        {
            Debug.LogError("ActionQueue out of Index");
        }
    }

    public void Action_Pass()
    {
        RequestControlStateChange(ControlState.Buffer);
    }

    public void Action_QuickMove_W()
    {
        tempObject = Instantiate(PrefabMaster.instance.contract, targeter.transform.position + Vector3.forward, Quaternion.identity);
        tempObject.GetComponent<Contract>().SetUpContract(currentSelection);
        tempObject = null;
        currentSelection = null;
        RequestControlStateChange(ControlState.Buffer);
    }

    public void Action_QuickMove_A()
    {
        tempObject = Instantiate(PrefabMaster.instance.contract, targeter.transform.position + Vector3.left, Quaternion.identity);
        tempObject.GetComponent<Contract>().SetUpContract(currentSelection);
        tempObject = null;
        currentSelection = null;
        RequestControlStateChange(ControlState.Buffer);
    }

    public void Action_QuickMove_S()
    {
        tempObject = Instantiate(PrefabMaster.instance.contract, targeter.transform.position + Vector3.back, Quaternion.identity);
        tempObject.GetComponent<Contract>().SetUpContract(currentSelection);
        tempObject = null;
        currentSelection = null;
        RequestControlStateChange(ControlState.Buffer);
    }

    public void Action_QuickMove_D()
    {
        tempObject = Instantiate(PrefabMaster.instance.contract, targeter.transform.position + Vector3.right, Quaternion.identity);
        tempObject.GetComponent<Contract>().SetUpContract(currentSelection);
        tempObject = null;
        currentSelection = null;
        RequestControlStateChange(ControlState.Buffer);
    }

    public void Action_QuickMove_Space()
    {
        tempObject = Instantiate(PrefabMaster.instance.contract, targeter.transform.position, Quaternion.identity);
        tempObject.GetComponent<Contract>().SetUpContract(currentSelection);
        tempObject = null;
        currentSelection = null;
        RequestControlStateChange(ControlState.Buffer);
    }

    public void Action_SelectUnit()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Action_SelectUnit_QuickSelect(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Action_SelectUnit_QuickSelect(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Action_SelectUnit_QuickSelect(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            Action_SelectUnit_QuickSelect(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            Action_SelectUnit_QuickSelect(5);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            Action_SelectUnit_QuickSelect(6);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            Action_SelectUnit_QuickSelect(7);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            Action_SelectUnit_QuickSelect(8);
        }
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            Action_SelectUnit_QuickSelect(9);
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            Action_SelectUnit_QuickSelect(0);
        }

    }

    public void Action_SelectUnit_QuickSelect(int desiredUnit)
    {
        SortAllies();
        if (desiredUnit <= allies.Count)
        {
            targeter.transform.position = allies[desiredUnit - 1].transform.position;
        }
    }

    public void AdjustMouseSensitivity()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus) || Input.GetKeyDown(KeyCode.Plus))
        {
            viewSensitivity += 2.5f;
            Debug.Log("increasing mouse sensitivity to factor " + viewSensitivity);
        }
        if ((Input.GetKeyDown(KeyCode.KeypadMinus) || Input.GetKeyDown(KeyCode.Minus)) && viewSensitivity > 2.5f)
        {
            viewSensitivity -= 2.5f;
            Debug.Log("Decreasing mouse sensitivity to factor " + viewSensitivity);
        }
    }

    public int CappedValue(int valueA, int valueB)
    {
        int sum = valueA + valueB;

        if (sum > gridLengthX - 1)
        {
            sum = 9;
        }
        if (sum < 0)
        {
            sum = 0;
        }
        return sum;
    }

    public void Cheats()
    {
        if (Input.GetKeyDown("k"))
        {
            for (int i = 1; i <= enemies.Count; i++)
            {
                i--;
                enemies[i].GetComponent<UnitBehaviour>().TakeDamage();
                if (enemies.Count < 1)
                {
                    i = 3;
                }
            }
        }

        if (Input.GetKeyDown("l"))
        {
            for (int i = 1; i <= allies.Count; i++)
            {
                i--;
                allies[i].GetComponent<UnitBehaviour>().TakeDamage();
                if (allies.Count < 1)
                {
                    i = 3;
                }
            }
        }

        if (Input.GetKeyDown("v"))
        {
            tde.Play_GameplayResponse(TDE.GameplayResponseType.UnitDeath);
        }
    }

    public void CheckWinCondition()
    {
        if (enemies.Count == 0)
        {
            RequestGameStateChange(GameState.Transition);
        }
        if (allies.Count == 0)
        {
            numberOfRuns++;
            runActive = false;

            SaveState_Save();
            SceneManager.LoadScene(0);
        }
    }

    public void Debugging()
    {
        Debug_ShowInterface();
        Debug_ShowGrid();
        Debug_UpdateGridInterface();
        UpdateDebugText();
    }

    public string Debug_GridLineToString(int z)
    {
        string line = "_";
        for (int i = 0; i < gridLengthX; i++)
        {
            if (gp[i, z] != null)
            {
                line += gp[i, z].name.ToString();
                line += " ";
            }
            else
            {
                line += "    empty   ";
            }
        }
        return line;
    }

    public void Debug_PrintGridToConsole()
    {
        for (int i = gridLengthX - 1; i >= 0; i--)
        {
            Debug.LogWarning(Debug_GridLineToString(i));
        }
    }

    public void Debug_ShowGrid()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            if (gridInterface.activeInHierarchy == true)
            {
                gridInterface.SetActive(false);
            }
            else
            {
                gridInterface.SetActive(true);
            }
        }
    }

    public void Debug_ShowInterface()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (debugInterface.activeInHierarchy == true)
            {
                debugInterface.SetActive(false);
            }
            else
            {
                debugInterface.SetActive(true);
            }
        }
    }

    public void Debug_UpdateGridInterface()
    {
        if (gridInterface.activeInHierarchy)
        {
            for (int i = gridLengthY - 1; i >= 0; i--)
            {
                Text textElement = gridInterface.transform.GetChild(i).GetComponent<Text>();
                textElement.text = i.ToString() + "  ";
                for (int o = 0; o < gridLengthX; o++)
                {
                    textElement.text += gd[o, i];
                    textElement.text += " ";
                }
            }
        }
    }

    public void DecoyVoid()
    {
        // just a filler to check delegates for content;
    }

    public void EmptyGrid()
    {
        for (int i = gridLengthY - 1; i >= 0; i--)
        {
            for (int o = 0; o < gridLengthX; o++)
            {
                gd[o, i] = "00000" + o.ToString() + i.ToString();
                gp[o, i] = null;
                //Debug.Log("Grid Position " + o + " " + i +  " is now " + gd[o,i]);
            }
        }
        Debug.Log("Emptied all Grid Information to 0000000");
    }

    public void Execution_Allies()
    {
        Debug.Log("///////////////////////////////////////////// Start Scan Allies ////////////////////////////////////////");
        foreach (GameObject ally in allies)
        {
            UnitBehaviour ub = ally.GetComponent<UnitBehaviour>();
            ub.Act();
        }
    }

    public void Execution_Enemies()
    {
        Debug.Log("///////////////////////////////////////////// Start Scan Enemies ////////////////////////////////////////");
        foreach (GameObject enemy in enemies)
        {
            UnitBehaviour ub = enemy.GetComponent<UnitBehaviour>();
            ub.Act();
        }
    }

    public void Feedback()
    {
        feedbackCount++;
    }

    public void GameInit() // sets up the game
    {
        if (currentGameState == GameState.Init)
        {
            if (cameraObject != null)
            {
                cb = cameraObject.GetComponent<CameraBehaviour>();
            }
            else
            {
                cameraObject = GameObject.Find("Main Camera");
                cb = cameraObject.GetComponent<CameraBehaviour>();
            }

            if (targeter == null)
            {
                targeter = GameObject.Find("Targeter");
            }
            if (pauseCube == null)
            {
                pauseCube = GameObject.Find("PauseCube");
            }
            SaveState_Load();
            currentGameState = GameState.Ingame;
        }
    }

    void Ingame() // executes commands that are specific to GameState.Ingame
    {
        if (currentGameState == GameState.Ingame)
        {
            Targeting();
            //ToggleGridMode();
            CheckWinCondition();
            if (currentControlState == ControlState.Inputphase)
            {
                Action();
            }
            /*
            if (currentControlState == ControlState.Inputphase)
            {
                if (ActionQueue_Zero != DecoyVoid)
                {
                    ActionQueue_Zero();
                    ActionQueue_Zero = DecoyVoid;
                    if (ActionQueue_One != DecoyVoid)
                    {
                        ActionQueue_Zero = ActionQueue_One;
                        ActionQueue_One = DecoyVoid;
                        if (ActionQueue_Two != DecoyVoid)
                        {
                            ActionQueue_One = ActionQueue_Two;
                            ActionQueue_Two = DecoyVoid;
                        }
                    }
                    actionQueueIndex--;
                }
            }
            */
            if (currentControlState == ControlState.Execution)
            {
                if (turn == "allyturn")
                {
                    if (feedbackCount >= allies.Count)
                    {
                        Debug.Log("All allies gave feedback (" + feedbackCount + "/" + allies.Count + ") Requesting ControlStateChange(EnemyTurn)");
                        feedbackCount = 0;
                        RequestControlStateChange(ControlState.EnemyTurn);
                    }
                }
                if (turn == "enemyturn")
                {
                    if (feedbackCount >= enemies.Count)
                    {
                        Debug.Log("All enemies gave feedback (" + feedbackCount + "/" + enemies.Count + ") Requesting ControlStateChange(InputPhase)");
                        feedbackCount = 0;
                        RequestControlStateChange(ControlState.Inputphase);
                    }
                }
            }
            if (currentControlState == ControlState.Buffer)
            {
                if (turn == "allyturn")
                {
                    if (feedbackCount >= allies.Count)
                    {
                        Debug.Log("All allies gave feedback (" + feedbackCount + "/" + allies.Count + ") Requesting ControlStateChange(Execution)");
                        feedbackCount = 0;
                        RequestControlStateChange(ControlState.Execution);
                    }
                }
                else
                {
                    if (feedbackCount == enemies.Count)
                    {
                        Debug.Log("All enemies gave feedback (" + feedbackCount + "/" + enemies.Count + ") Requesting ControlStateChange(Execution)");
                        feedbackCount = 0;
                        RequestControlStateChange(ControlState.Execution);
                    }
                }
                // increase timer to wait for action to finish
            }
        }
    }

    public bool isAlly(int x, int z)
    {
        if (gp[x, z] != null)
        {
            currentSelection = gp[x, z];
            Debug.Log("currentSelection is " + currentSelection.name);
            if (currentSelection.tag == "Unit")
            {
                if (currentSelection.GetComponent<UnitBehaviour>().affiliation == 0)
                {
                    return true;
                }
            }

        }
        return false;
    }

    public bool NoActionInput()
    {
        if (ready_Ability)
        {
            return false;
        }
        else if (ready_Coin)
        {
            return false;
        }
        else if (ready_Contract)
        {
            return false;
        }
        else if (ready_Resurrection)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void Pause() // checks for escape input and changes the gamestate accordingly
    {
        if (currentGameState == GameState.Paused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                pauseCube.GetComponent<PauseCube>().NextMenu(true);
            }
            if (Input.GetMouseButtonDown(1))
            {
                pauseCube.GetComponent<PauseCube>().NextMenu(false);
            }

            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
            {
                currentGameState = GameState.Ingame;
                cb.SetObjectofInterest(targeter);
                //pauseCube.transform.position = Vector3.MoveTowards(pauseCube.transform.position, cb.eye.transform.position + Vector3.forward * 5 + Vector3.up * 10, 50f * Time.deltaTime);
                return;
            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(0);
            }
            if (Input.GetKeyDown(KeyCode.X))
            {
                SaveState save = new SaveState
                {
                    allyCount = 0,
                    enemyCount = 0,
                    obstacleCount = 0,
                    allyPositions = new Vector3[0],
                    enemyPositions = new Vector3[0],
                    obstaclePositions = new Vector3[0],
                    allyTypes = new string[0],
                    enemyTypes = new string[0],
                    stageIndex = 0,
                    activeRun = false,
                    sNumberOfRuns = 0,
                    currency = 3,
                    targeterSensitivity = viewSensitivity,
                };
                json = JsonUtility.ToJson(save);
                File.WriteAllText(Application.dataPath + "/save.txt", json);
                SceneManager.LoadScene(0);
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Application.Quit();
            }
        }
        if (currentGameState == GameState.Ingame)
        {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
            {
                currentGameState = GameState.Paused;
                pauseCube.transform.position = new Vector3(4.5f, 20, 4.5f); //cb.eye.transform.position + cb.eye.transform.forward + Vector3.up * 20;
                cb.SetObjectofInterest(pauseCube);
                return;
            }
        }

        if (currentGameState == GameState.Paused)
        {
            pauseCube.transform.position = Vector3.MoveTowards(pauseCube.transform.position, new Vector3(4.5f, 3f, 4.5f), 15f * Time.deltaTime / Time.timeScale);
        }
        else
        {
            pauseCube.transform.position = Vector3.MoveTowards(pauseCube.transform.position, new Vector3(4.5f, 50, 4.5f), 5f * Time.deltaTime / Time.timeScale);
        }
    }

    public void Payment_Abortion()
    {
        money += floatingCost[0];
        floatingCost.Clear();
        Payment_UpdateMoneyText();
    }

    public void Payment_Transaction()
    {
        floatingCost.Clear();
        Payment_UpdateMoneyText();
    }

    public void Payment_Initiation(int cost)
    {
        floatingCost.Add(cost);
        money -= cost;
        Payment_UpdateMoneyText();
    }

    public void Payment_UpdateMoneyText()
    {
        if (coinHolder == null || coinAnim == null)
        {
            coinHolder = GameObject.Find("Coin_Model");
            coinAnim = coinHolder.GetComponent<Animator>();
        }
        coinAnim.Play("CoinAnim");
        moneyText.text = money.ToString();
    }

    public void RequestControlStateChange(ControlState desiredState)
    {
        lastControlState = currentControlState;
        if (desiredState == ControlState.Dialogue)
        {
            tde.StartDialogue();
            Debug.Log("Controlstate has changed from " + lastControlState + " to " + currentControlState);
            return;
        }
        if (lastControlState == ControlState.Inputphase && desiredState == ControlState.Buffer)
        {
            tde.Play_GameplayResponse(TDE.GameplayResponseType.Command);
            feedbackCount = 0;
            Execution_Allies();
            turn = "allyturn";
        }
        if (lastControlState == ControlState.Execution && desiredState == ControlState.EnemyTurn)
        {
            feedbackCount = 0;
            Execution_Enemies();
            turn = "enemyturn";
            desiredState = ControlState.Buffer;
        }
        if (lastControlState == ControlState.Execution && desiredState == ControlState.Inputphase)
        {
            feedbackCount = 0;
            turn = "allyturn";
            SaveState_Save();
        }
        currentControlState = desiredState;
        Debug.Log("Controlstate has changed from " + lastControlState + " to " + currentControlState);
        //UpdateDebugText();
    }

    public void RequestGameStateChange(GameState desiredState)
    {
        lastGameState = currentGameState;
        currentGameState = desiredState;
    }

    public void ResurrectionDestroyOriginal(string type)
    {
        resurrectionRayCastHits = Physics.BoxCastAll(targeter.transform.position, new Vector3(0.25f, 0.25f, 0.25f), Vector3.down);
        for (int i = 0; i < resurrectionRayCastHits.Length; i++)
        {
            if (resurrectionRayCastHits[i].collider.gameObject.tag == "Corpse")
            {
                if (resurrectionRayCastHits[i].collider.gameObject.GetComponent<UnitBehaviour>().unitType == type)
                {
                    Destroy(resurrectionRayCastHits[i].collider.gameObject);
                    i = 99;
                }
            }
        }
    }

    public GameObject ResurrectionSpawnCorresponding(GameObject impression)
    {
        string type = impression.GetComponent<UnitBehaviour>().unitType;
        if (type == "Base")
        {
            ResurrectionDestroyOriginal(type);
            Payment_Initiation(impression.GetComponent<UnitBehaviour>().resurrectionCost);
            Payment_Transaction();
            return PrefabMaster.instance.zombie_Base;
        }
        if (type == "Force")
        {
            ResurrectionDestroyOriginal(type);
            Payment_Initiation(impression.GetComponent<UnitBehaviour>().resurrectionCost);
            Payment_Transaction();
            return PrefabMaster.instance.zombie_Force;
        }
        if (type == "Spear")
        {
            ResurrectionDestroyOriginal(type);
            Payment_Initiation(impression.GetComponent<UnitBehaviour>().resurrectionCost);
            Payment_Transaction();
            return PrefabMaster.instance.zombie_Spear;
        }
        else
        {
            ResurrectionDestroyOriginal(type);
            Payment_Initiation(impression.GetComponent<UnitBehaviour>().resurrectionCost);
            Payment_Transaction();
            return PrefabMaster.instance.zombie_Base;
        }
    }

    public void ResurrectionTargets_Show()
    {
        resurrectionRayCastHits = Physics.BoxCastAll(targeter.transform.position, new Vector3(0.25f, 0.25f, 0.25f), Vector3.down);
        for (int i = 0; i < resurrectionRayCastHits.Length; i++)
        {
            if (resurrectionRayCastHits[i].collider.gameObject.tag == "Unit")
            {
                if (resurrectionRayCastHits[i].collider.gameObject.GetComponent<UnitBehaviour>().affiliation == 0)
                {
                    resurrectionTargets.Add(resurrectionRayCastHits[i].collider.gameObject);
                }
            }
            if (resurrectionRayCastHits[i].collider.gameObject.tag == "Corpse")
            {
                resurrectionTargets.Add(resurrectionRayCastHits[i].collider.gameObject);
            }
        }
        resurrectionShowReel.Clear();
        for (int i = 0; i < resurrectionTargets.Count; i++)
        {
            resurrectionShowReel.Add(Instantiate(resurrectionTargets[i], targeter.transform.position + resurrectionShowReelPositions[i] + Vector3.left * 0.5f * (resurrectionTargets.Count - 1), new Quaternion(0,1,0,0)));
            resurrectionShowReel[i].GetComponent<UnitBehaviour>().enabled = false;
            resurrectionShowReel[i].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        if (resurrectionShowReel.Count > 0)
        {
            cb.SetObjectofInterest(resurrectionShowReel[0]);
            selectionCircle = Instantiate(PrefabMaster.instance.selectionCircle, resurrectionShowReel[0].transform.position, Quaternion.identity);
            selectionCircle.transform.Rotate(0, 0, 0);
        }
        else
        {
            ready_Resurrection = false;
            resurrectionTargets.Clear();
            resurrectionShowReel.Clear();
        }
    }

    public void ResurrectionTargets_Show_Advanced()
    {
        // determin options from inventory
        if (resurrectionShowReel[resurrectionShowReelIndex].tag == "Corpse")
        {
            resurrectionShowReel_Advanced.Add(Instantiate(PrefabMaster.instance.zombie_Base, resurrectionShowReel[resurrectionShowReelIndex].transform.position + resurrectionShowReelPositions_Advanced[resurrectionShowReel_Advanced.Count] + Vector3.left * 0.5f * (resurrectionShowReel_Advanced.Count), new Quaternion(0, 1, 0, 0)));
            resurrectionShowReel_Advanced[resurrectionShowReel_Advanced.Count - 1].GetComponent<UnitBehaviour>().enabled = false;
            resurrectionShowReel_Advanced[resurrectionShowReel_Advanced.Count - 1].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            resurrectionShowReel_Advanced.Add(Instantiate(resurrectionShowReel[resurrectionShowReelIndex], resurrectionShowReel[resurrectionShowReelIndex].transform.position + resurrectionShowReelPositions_Advanced[resurrectionShowReel_Advanced.Count] + Vector3.left * 0.5f * (resurrectionShowReel_Advanced.Count), new Quaternion(0, 1, 0, 0)));
            resurrectionShowReel_Advanced[resurrectionShowReel_Advanced.Count - 1].GetComponent<UnitBehaviour>().enabled = false;
            resurrectionShowReel_Advanced[resurrectionShowReel_Advanced.Count - 1].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            cb.SetObjectofInterest(resurrectionShowReel_Advanced[0]);
            selectionCircle.transform.position = resurrectionShowReel_Advanced[0].transform.position;
        }
        if (resurrectionShowReel[resurrectionShowReelIndex].tag == "Unit")
        {
            resurrectionShowReel_Advanced.Add(Instantiate(PrefabMaster.instance.noUpgradeBlock, resurrectionShowReel[resurrectionShowReelIndex].transform.position + resurrectionShowReelPositions_Advanced[resurrectionShowReel_Advanced.Count] + Vector3.left * 0.5f * (resurrectionShowReel_Advanced.Count), Quaternion.identity));
            resurrectionShowReel_Advanced[resurrectionShowReel_Advanced.Count - 1].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
    }

    public void ResurrectionTargets_Remove()
    {
        for (int i = 0; i < resurrectionShowReel.Count; i++)
        {
            Destroy(resurrectionShowReel[i]);
        }
        resurrectionShowReelIndex = 0;
        resurrectionTargets.Clear();
        resurrectionShowReel.Clear();
        cb.SetObjectofInterest(targeter);
        Destroy(selectionCircle);
        ready_Resurrection = false;
    }

    public void ResurrectionTargets_Remove_Advanced()
    {
        for (int i = 0; i < resurrectionShowReel_Advanced.Count; i++)
        {
            Destroy(resurrectionShowReel_Advanced[i]);
        }
        resurrectionShowReelIndex_Advanced = 0;
        resurrectionShowReel_Advanced.Clear();
        cb.SetObjectofInterest(resurrectionShowReel[resurrectionShowReelIndex]);
        selectionCircle.transform.position = resurrectionShowReel[resurrectionShowReelIndex].transform.position;
        ready_Resurrection_Advanced = false;
    }

    public class SaveState
    {
        public int allyCount;
        public int enemyCount;
        public int obstacleCount;
        public string[] allyTypes = new string[0];
        public string[] enemyTypes = new string[0];
        public Vector3[] allyPositions = new Vector3[0];
        public Vector3[] enemyPositions = new Vector3[0];
        public Vector3[] obstaclePositions = new Vector3[0];
        public int stageIndex;
        public int currency;
        public bool activeRun;
        public int sNumberOfRuns;

        public float targeterSensitivity;
    }

    public void SaveState_Save()
    {
        Vector3[] sAllyPositions = new Vector3[allies.Count];
        string[] sAllyTypes = new string[allies.Count];
        if (allies.Count > 0)
        {
            runActive = true;
            for (int i = 0; i < allies.Count; i++)
            {
                sAllyPositions[i] = allies[i].transform.position;
            }
            for (int i = 0; i < allies.Count; i++)
            {
                sAllyTypes[i] = allies[i].GetComponent<UnitBehaviour>().unitType;
            }
        }
        else
        {
            foreach (GameObject ally in allies)
            {
                Destroy(ally);
            }
            allies.Clear();
        }

        Vector3[] sEnemyPositions = new Vector3[enemies.Count];
        string[] sEnemyTypes = new string[enemies.Count];
        if (enemies.Count > 0)
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                sEnemyPositions[i] = enemies[i].transform.position;
            }
            for (int i = 0; i < enemies.Count; i++)
            {
                sEnemyTypes[i] = enemies[i].GetComponent<UnitBehaviour>().unitType;
            }
        }
        else
        {
            foreach (GameObject enemy in enemies)
            {
                Destroy(enemy);
            }
            enemies.Clear();
        }

        /*
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        List<GameObject> oList = new List<GameObject>();
        foreach(GameObject obj in obstacles)
        {
            oList.Add(obj);
        }
        Vector3[] oPos = new Vector3[oList.Count];
        for (int i = 0; i < oList.Count - 1; i++)
        {
            oPos[i] = oList[i].transform.position;
        }
        */


        SaveState save = new SaveState
        {
            allyCount = allies.Count,
            enemyCount = enemies.Count,
            //obstacleCount = obstacles.Length,
            allyPositions = sAllyPositions,
            enemyPositions = sEnemyPositions,
            //obstaclePositions = oPos,
            allyTypes = sAllyTypes,
            enemyTypes = sEnemyTypes,
            stageIndex = levelIndex,
            activeRun = runActive,
            sNumberOfRuns = numberOfRuns,
            currency = money,
            targeterSensitivity = viewSensitivity,
        };
        json = JsonUtility.ToJson(save);
        File.WriteAllText(Application.dataPath + "/save.txt", json);
        Debug.Log("Saved GameState : " + json);
    }

    public void SaveState_Load()
    {
        if (File.Exists(Application.dataPath + "/save.txt"))
        {
            string saveString = File.ReadAllText(Application.dataPath + "/save.txt");
            SaveState load = JsonUtility.FromJson<SaveState>(saveString);
            json = JsonUtility.ToJson(load);
            viewSensitivity = load.targeterSensitivity;
            //create empty field
            //track corpses at a later point
            //track trees at a later point
            if (load.activeRun)
            {
                foreach(GameObject ally in allies)
                {
                    Destroy(ally);
                }
                allies.Clear();
                foreach(GameObject enemy in enemies)
                {
                    Destroy(enemy);
                }
                enemies.Clear();
                for (int i = 0; i < load.allyCount; i++)
                {
                    GameObject a = Instantiate(SpawnAlly(load.allyTypes[i]), load.allyPositions[i], Quaternion.identity);
                    a.GetComponent<UnitBehaviour>().Register();
                }
                if (load.enemyCount == 0)
                {
                    RequestGameStateChange(GameState.Transition);
                }
                for (int i = 0; i < load.enemyCount; i++)
                {
                    GameObject e = Instantiate(SpawnEnemy(load.enemyTypes[i]), load.enemyPositions[i], Quaternion.identity);
                    e.transform.Rotate(0, 180, 0);
                    e.GetComponent<UnitBehaviour>().Register();
                }
                money = load.currency;
                numberOfRuns = load.sNumberOfRuns;

                /*
                GameObject[] o = GameObject.FindGameObjectsWithTag("Obstacle");
                foreach(GameObject ob in o)
                {
                    Destroy(ob);
                }
                if (load.obstacleCount > 0)
                {
                    Debug.Log(load.obstacleCount + " / " + load.obstaclePositions[0]);
                    for (int i = 0; i < load.obstacleCount - 1; i++)
                    {
                        GameObject ob = Instantiate(PrefabMaster.instance.tree, load.obstaclePositions[i], Quaternion.identity);
                        ob.GetComponent<Obstacle>().Register();
                    }
                }
                */

            }
            else
            {
                numberOfRuns = load.sNumberOfRuns;
                // other things passed on beyond playthroughs
                Debug.Log("No active Run, creating new run; Run number : " + load.sNumberOfRuns);
            }
        }
        else
        {
            Debug.Log("No save detected");
            //Create scene from scratch.
        }

        ToggleGridMode_On();
        Payment_UpdateMoneyText();
        Debug.Log("Load GameState : " + json);
    }

    public GameObject SpawnAlly(string type)
    {
        GameObject go;
        if (type == "Base")
        {
            go = PrefabMaster.instance.zombie_Base;
            return go;
        }
        if (type == "Force")
        {
            go = PrefabMaster.instance.zombie_Force;
            return go;
        }
        if (type == "Spear")
        {
            go = PrefabMaster.instance.zombie_Spear;
            return go;
        }
        else
        {
            go = PrefabMaster.instance.zombie_Base;
            return go;
        }
    }

    public GameObject SpawnEnemy(string type)
    {
        GameObject go;
        if (type == "Base")
        {
            go = PrefabMaster.instance.knight_Base;
            return go;
        }
        if (type == "Force")
        {
            go = PrefabMaster.instance.knight_Force;
            return go;
        }
        if (type == "Spear")
        {
            go = PrefabMaster.instance.knight_Spear;
            return go;
        }
        else
        {
            go = PrefabMaster.instance.knight_Base;
            return go;
        }
    }

    public void SpawnCoin(int x, int z)
    {
        currentCoin = Instantiate(PrefabMaster.instance.coin, new Vector3(x, 0, z), Quaternion.identity);
        gp[x, z] = currentCoin;
        gd[x, z] = "60000" + x.ToString() + z.ToString();
    }

    public void SetMoveProxy(int x, int z) // used by units to mark their next destination as not pathable for other units
    {
        gp[x, z] = moveProxy;
        Debug.Log(gp[x, z].name + " is now placed at " + x + z);
    }

    public void SortUnits() // sorts both allies and enemies by : furthest ahead (highest z for allies, lowest z for enemies), then furthest to the left
    {
        SortAllies();
        SortEnemies();
    }

    public void SortAllies()
    {
        List<GameObject> tempAllies = allies;
        List<GameObject> tempComparison = new List<GameObject>();
        List<GameObject> sortedAllies = new List<GameObject>();
        int highestZ = 0;
        int lowestX = 10;
        GameObject lowestXholder = null;
        while (tempAllies.Count > 0)
        {
            foreach (GameObject ally in tempAllies)
            {
                if ((int)ally.transform.position.z > highestZ)
                {
                    //Debug.Log("Found new highest Z at " + (int)ally.transform.position.z + " for " + ally.name);
                    tempComparison.Clear();
                    tempComparison.Add(ally);
                    highestZ = (int)ally.transform.position.z;
                }
                else if ((int)ally.transform.position.z == highestZ)
                {
                    //Debug.Log("Adding to comparison: " + ally.name);
                    tempComparison.Add(ally);
                }
            }
            if (tempComparison.Count > 1)
            {
                //Debug.Log("Comparing highest z's  for lowest X");
                lowestX = 10;
                while (tempComparison.Count > 0)
                {
                    foreach (GameObject allyX in tempComparison)
                    {
                        if (allyX.transform.position.x < lowestX)
                        {
                            lowestXholder = allyX;
                            lowestX = (int)allyX.transform.position.x;
                        }
                    }
                    sortedAllies.Add(lowestXholder);
                    tempAllies.Remove(lowestXholder);
                    tempComparison.Remove(lowestXholder);
                    lowestX = 10;
                }
                highestZ = 0;
            }
            else
            {
                //Debug.Log("Only one contender. Converting " + tempComparison[0].name);
                sortedAllies.Add(tempComparison[0]);
                tempAllies.Remove(tempComparison[0]);
                tempComparison.Clear();
                highestZ = 0;
            }
        }
        allies = sortedAllies;
    }

    public void SortEnemies()
    {
        List<GameObject> tempEnemies = enemies;
        List<GameObject> tempComparison = new List<GameObject>();
        List<GameObject> sortedEnemies = new List<GameObject>();
        int lowestZ = 10;
        int lowestX = 10;
        GameObject lowestXholder = null;
        while (tempEnemies.Count > 0)
        {
            foreach (GameObject enemy in tempEnemies)
            {
                if ((int)enemy.transform.position.z < lowestZ)
                {
                    Debug.Log("Found new lowest Z at " + (int)enemy.transform.position.z + " for " + enemy.name);
                    tempComparison.Clear();
                    tempComparison.Add(enemy);
                    lowestZ = (int)enemy.transform.position.z;
                }
                else if ((int)enemy.transform.position.z == lowestZ)
                {
                    Debug.Log("Adding to comparison: " + enemy.name);
                    tempComparison.Add(enemy);
                }
            }
            Debug.Log(tempComparison.Count);
            Debug.Log(tempComparison[0].name);
            if (tempComparison.Count > 1)
            {
                Debug.Log("Comparing highest z's  for lowest X");
                lowestX = 10;
                while (tempComparison.Count > 0)
                {
                    foreach (GameObject enemyX in tempComparison)
                    {
                        if (enemyX.transform.position.x < lowestX)
                        {
                            lowestXholder = enemyX;
                            lowestX = (int)enemyX.transform.position.x;
                        }
                    }
                    sortedEnemies.Add(lowestXholder);
                    tempEnemies.Remove(lowestXholder);
                    tempComparison.Remove(lowestXholder);
                    lowestX = 10;
                }
                lowestZ = 10;
            }
            else
            {
                Debug.Log("Only one contender. Converting " + tempComparison[0].name);
                sortedEnemies.Add(tempComparison[0]);
                tempEnemies.Remove(tempComparison[0]);
                tempComparison.Clear();
                lowestZ = 10;
            }
        }
        enemies = sortedEnemies;
    }

    void Targeting() // checks mouseinput and moves the viewanker of the camera along the gp of the playfield
    {
        mouseXdif += Input.GetAxis("Mouse X") * Time.deltaTime * viewSensitivity / currentTimeScale;
        mouseYdif += Input.GetAxis("Mouse Y") * Time.deltaTime * viewSensitivity * 2 / currentTimeScale;

        if ((mouseXdif >= 1) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            mouseXdif = 0;
            if (ready_Resurrection && !ready_Resurrection_Advanced)
            {
                resurrectionShowReelIndex += 1;
                if (resurrectionShowReelIndex < resurrectionShowReel.Count)
                {
                    cb.SetObjectofInterest(resurrectionShowReel[resurrectionShowReelIndex]);
                    selectionCircle.transform.position = resurrectionShowReel[resurrectionShowReelIndex].transform.position;
                }
                else
                {
                    resurrectionShowReelIndex -= 1;
                }
            }
            else if (ready_Resurrection && ready_Resurrection_Advanced)
            {
                resurrectionShowReelIndex_Advanced += 1;
                if (resurrectionShowReelIndex_Advanced < resurrectionShowReel_Advanced.Count)
                {
                    cb.SetObjectofInterest(resurrectionShowReel_Advanced[resurrectionShowReelIndex_Advanced]);
                    selectionCircle.transform.position = resurrectionShowReel_Advanced[resurrectionShowReelIndex_Advanced].transform.position;
                }
                else
                {
                    resurrectionShowReelIndex_Advanced -= 1;
                }
            }
            else
            {
                if (targeter.transform.position.x < gridLengthX - 1)
                {
                    targeter.transform.Translate(1, 0, 0);
                }
            }
        }
        if ((mouseXdif < -1) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            mouseXdif = 0;
            if (ready_Resurrection && !ready_Resurrection_Advanced)
            {
                resurrectionShowReelIndex -= 1;
                if (resurrectionShowReelIndex >= 0)
                {
                    cb.SetObjectofInterest(resurrectionShowReel[resurrectionShowReelIndex]);
                    selectionCircle.transform.position = resurrectionShowReel[resurrectionShowReelIndex].transform.position;
                }
                else
                {
                    resurrectionShowReelIndex += 1;
                }
            }
            else if (ready_Resurrection && ready_Resurrection_Advanced)
            {
                resurrectionShowReelIndex_Advanced -= 1;
                if (resurrectionShowReelIndex_Advanced >= 0)
                {
                    cb.SetObjectofInterest(resurrectionShowReel_Advanced[resurrectionShowReelIndex_Advanced]);
                    selectionCircle.transform.position = resurrectionShowReel_Advanced[resurrectionShowReelIndex_Advanced].transform.position;
                }
                else
                {
                    resurrectionShowReelIndex_Advanced += 1;
                }
            }
            else
            {
                if (targeter.transform.position.x > 0)
                {
                    targeter.transform.Translate(-1, 0, 0);
                }
            }
        }

        if ((mouseYdif > 1) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            mouseYdif = 0;
            if (ready_Resurrection)
            {

            }
            else
            {
                if (targeter.transform.position.z < gridLengthY - 1)
                {
                    targeter.transform.Translate(0, 0, 1);
                }
            }
        }

        if ((mouseYdif < -1) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            mouseYdif = 0;
            if (ready_Resurrection)
            {

            }
            else
            {
                if (targeter.transform.position.z > 0)
                {
                    targeter.transform.Translate(0, 0, -1);
                }
            }
        }
    }

    public void TimeScaleModify()
    {
        /*
        if (desiredTimeScale > Time.timeScale + 0.01f)
        {
            Time.timeScale += timeScaleGrowth * Time.deltaTime / Time.timeScale;
            currentTimeScale = Time.timeScale;
        }
        else if (desiredTimeScale < Time.timeScale - 0.01f)
        {
            Time.timeScale -= timeScaleGrowth * Time.deltaTime / Time.timeScale;
            currentTimeScale = Time.timeScale;
        }
        */
    }
    public void ToggleGridMode()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            ToggleGridMode_On();
            gridToggleStay = false;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            gridToggleCounter += Time.deltaTime / Time.timeScale;
            if (gridToggleCounter > 0.5f)
            {
                gridToggleStay = true;
            }
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            if (gridToggleStay)
            {
                ToggleGridMode_Off();
            }
            gridToggleCounter = 0;
            gridToggleStay = false;
        }
    }

    public void ToggleGridMode_On()
    {
        gridSF.FadeIn();
        foreach (GameObject ally in allies)
        {
            ally.transform.GetChild(1).GetComponent<SpriteFade>().FadeIn();
        }
        foreach (GameObject enemy in enemies)
        {
            enemy.transform.GetChild(1).GetComponent<SpriteFade>().FadeIn();
        }
    }

    public void ToggleGridMode_Off()
    {
        gridSF.FadeOut();
        foreach (GameObject ally in allies)
        {
            ally.transform.GetChild(1).GetComponent<SpriteFade>().FadeOut();
        }
        foreach (GameObject enemy in enemies)
        {
            enemy.transform.GetChild(1).GetComponent<SpriteFade>().FadeOut();
        }
    }

    public void Transition()
    {
        if (currentGameState == GameState.Transition)
        {
            if (!nextSegmentinstantiated)
            {
                newTerrainElevated = false;
                ToggleGridMode_Off();
                money += 2;
                Payment_UpdateMoneyText();
                cameraObject.GetComponent<CameraBehaviour>().eye.transform.position = transitionSpots[Random.Range(0, 3)];
                GameObject[] cubes = GameObject.FindGameObjectsWithTag("TerrainCube");
                foreach(GameObject cube in cubes)
                {
                    if (Vector3.Distance(cube.transform.position, Vector3.zero) < 20)
                    {
                        cube.transform.parent = oldSegment.transform;
                    }
                }
                newSegment = Instantiate(PrefabMaster.instance.NewTerrain(), newTerrainSpawnLocation, Quaternion.identity);
                Debug.Log("New segment instantiated");
                nextSegmentinstantiated = true;
                tde.Play_Transition();
                newAllyPositions = new Vector3[allies.Count];
                foreach (GameObject obstacle in gp)
                {
                    if (obstacle != null)
                    {
                        if (obstacle.tag == "Obstacle")
                        {
                            obstacle.transform.parent = oldSegment.transform;
                        }
                    }
                }
                for (int i = 0; i < allies.Count; i++)
                {
                    if (gp[(int)allies[i].transform.position.x, (int)allies[i].transform.position.z / 2] == null)
                    {
                        newAllyPositions[i] = new Vector3(allies[i].transform.position.x, 0, Mathf.FloorToInt(allies[i].transform.position.z / 2));
                    }
                    else if (gp[(int)allies[i].transform.position.x, Mathf.FloorToInt((int)allies[i].transform.position.z / 2 + 1)] == null)
                    {
                        newAllyPositions[i] = new Vector3(allies[i].transform.position.x, 0, Mathf.FloorToInt(allies[i].transform.position.z / 2 + 1));
                    }
                    else
                    {
                        newAllyPositions[i] = new Vector3(allies[i].transform.position.x, 0, Mathf.FloorToInt(allies[i].transform.position.z / 2 + 2));
                    }
                   
                }
            }
            else
            {
                if (!newTerrainElevated)
                {
                    newSegment.transform.position = Vector3.MoveTowards(newSegment.transform.position, oldSegment.transform.position + Vector3.forward * (10 - Time.deltaTime), transitionSpeed * newSegmentSpeed * Time.deltaTime);
                    if (Vector3.Distance(newSegment.transform.position, newTerrainPosition) < 0.1f)
                    {
                        newTerrainElevated = true;
                    }
                }
                if (newTerrainElevated)
                {
                    newSegment.transform.position = Vector3.MoveTowards(newSegment.transform.position, terrainMainPosition, transitionSpeed * Time.deltaTime);
                }
                oldSegment.transform.position = Vector3.MoveTowards(oldSegment.transform.position, oldTerrainPosition, transitionSpeed * Time.deltaTime);
                if (trashSegment != null)
                {
                    trashSegment.transform.position = Vector3.MoveTowards(trashSegment.transform.position, trashTerrainPosition, transitionSpeed * Time.deltaTime);
                }
                
                for (int i = 0; i < allies.Count; i++)
                {
                    allies[i].transform.position = Vector3.MoveTowards(allies[i].transform.position, newAllyPositions[i], transitionSpeed * Time.deltaTime);
                    if (Vector3.Distance(allies[i].transform.position, newAllyPositions[i]) < 0.2f)
                    {
                        allies[i].GetComponent<UnitAnim>().OnMove();
                        allies[i].transform.rotation = new Quaternion(0, 0, 0, 0);
                    }
                }
                
                if (Vector3.Distance(newSegment.transform.position, terrainMainPosition) <= 0.1f)
                {
                    Debug.Log("Finalizing positions");
                    newSegment.transform.position = terrainMainPosition;
                    oldSegment.transform.position = oldTerrainPosition;

                    for (int i = 0; i < allies.Count; i++)
                    {
                        allies[i].transform.position = newAllyPositions[i];
                        allies[i].transform.GetChild(0).GetComponent<Animator>().Play("Idle");
                        allies[i].GetComponent<UnitAnim>().isMoving = false;
                    }

                    EmptyGrid();
                    GameObject[] tempAllies = new GameObject[allies.Count];
                    for (int i = 0; i < allies.Count; i++)
                    {
                        tempAllies[i] = allies[i];
                    }
                    allies.Clear();
                    foreach (GameObject unit in tempAllies)
                    {
                        unit.GetComponent<UnitBehaviour>().Register();
                    }
                    
                    for (int i = 1; i <= newSegment.transform.childCount; i++)
                    {
                        i--;
                        newSegment.transform.GetChild(i).SendMessage("Register", null, SendMessageOptions.DontRequireReceiver);
                        newSegment.transform.GetChild(i).transform.parent = null;
                    }

                    if (trashSegment != null)
                    {
                        Destroy(trashSegment);
                    }
                    trashSegment = oldSegment;
                    oldSegment = newSegment;
                    nextSegmentinstantiated = false;

                    SaveState_Save();
                    ToggleGridMode_On();
                    RequestGameStateChange(GameState.Ingame);
                    SortUnits();
                }
            }
        }
    }

    public void UpdateDebugText()
    {
        controlStateText.text = currentControlState.ToString();
        turnText.text = turn;
    }
}
