﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contract : MonoBehaviour
{
    public GameObject contractor;
    public UnitBehaviour cu;
    public Pathing cp;
    public GameObject condition;
    public int payoutvalue;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == contractor)
        {
            PayOut();
        }
        else
        {
            cu.target = other.gameObject;
            cp.target = other.gameObject;
            // should really make a setter function for updating targets..
            gameObject.transform.parent = other.gameObject.transform;
        }
    }

    public void PayOut()
    {
        // give payout to contractor
        cu.target = null;
        cp.target = null;
        cp.visualisationData = new Vector3[0];
        Debug.Log("Contract collected by " + contractor.name);
        Destroy(gameObject);
    }

    public void ReapplyPosition()
    {
        // have objects check wether their position is occupied, and add themselves to the grid again if not
    }

    public void SetUpContract(GameObject assignee)
    {
        contractor = assignee;
        cu = contractor.GetComponent<UnitBehaviour>();
        cp = contractor.GetComponent<Pathing>();
        cu.target = gameObject;
        cp.target = gameObject;
        //cp.RequestNewPath(0);
    }
}
