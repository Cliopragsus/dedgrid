﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int x;
    public int z;

    // Start is called before the first frame update
    void Start()
    {
        x = Mathf.RoundToInt(transform.position.x);
        z = Mathf.RoundToInt(transform.position.z);
        Debug.Log("Coin registered at " + x + z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("Coin was collected");
        Destroy(gameObject);
    }
}
