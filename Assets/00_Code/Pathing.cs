﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]

public class Pathing : MonoBehaviour
{
    public delegate void CheckAdjacent();
    public CheckAdjacent checkAdjacent;

    public GameObject target;

    public Vector3[] pathData = new Vector3[99];
    public Vector3[] visualisationData = new Vector3[99];
    public int targetX = 99;
    public int targetZ = 99;
    public Vector3 targetPos;
    public int currentX = 99;
    public int currentZ = 99;
    public Vector3 currentPos;
    public int calcX = 99; // virtual x coordinate that is used during path calculation
    public int calcZ = 99; // virtual z coordinate that is used during path calculation
    public Vector3 calcPos;

    public GameObject moveProxy;

    public GameObject[,] actualGrid = new GameObject[99, 99];
    public GameObject[,] virtualGrid = new GameObject[99, 99];

    private int calcIndex;

    private GameManager gm;
    private LineRenderer lr;
    private UnitBehaviour ub;

    // Start is called before the first frame update
    public void Start()
    {
        gm = GameManager.instance;
        moveProxy = gm.moveProxy;
        lr = gameObject.GetComponent<LineRenderer>();
        ub = gameObject.GetComponent<UnitBehaviour>();
        actualGrid = gm.gp;
        virtualGrid = actualGrid.Clone() as GameObject[,];
    }

    // Update is called once per frame
    void Update()
    {
        lr.positionCount = visualisationData.Length;
        lr.SetPositions(visualisationData);
        if (pathData[1] != Vector3.zero)
        {
            lr.SetPosition(0, transform.position);
        }
    }

    private void Calculate()
    {
        checkAdjacent();

        if (calcX == targetX && calcZ == targetZ)
        {
            currentX = calcX;
            currentZ = calcZ;
            currentPos = new Vector3(calcX, 0, calcZ);
            pathData[calcIndex] = currentPos;
            Debug.Log("calcX and calcZ reached target");
            visualisationData = new Vector3[calcIndex + 2];
            visualisationData[0] = transform.position;
            for (int i = 0; i <= calcIndex; i++)
            {
                visualisationData[i + 1] = pathData[i];
            }
        }
        else
        {
            currentX = calcX;
            currentZ = calcZ;
            currentPos = new Vector3(calcX, 0, calcZ);
            Debug.Log("target" + "(" + target.name + " " + targetX + targetZ + ")" + " not yet reached, calculating step " + calcIndex + " from " + currentPos);
            pathData[calcIndex] = currentPos;
            virtualGrid[calcX, calcZ] = moveProxy;
            calcIndex++;
            Calculate();
        }
    }

    public Vector3 RequestMove(int priority)
    {
        calcIndex = 0;
        pathData[0] = new Vector3(Mathf.RoundToInt(transform.position.x), 0, Mathf.RoundToInt(transform.position.z));
        currentPos = transform.position;
        currentX = (int)transform.position.x;
        currentZ = (int)transform.position.z;
        calcX = 99;
        calcZ = 99;
        actualGrid = gm.gp;
        virtualGrid = actualGrid.Clone() as GameObject[,];
        checkAdjacent = null;
        pathData = new Vector3[99];

        if (target != null)
        {
            RequestNewPath(priority + 1);
            return pathData[0];
        }
        else
        {
            DeterminePriority(priority);
            checkAdjacent();
            return new Vector3(calcX, 0, calcZ);
        }
    }

    public void RequestNewPath(int priority)
    {
        virtualGrid = actualGrid.Clone() as GameObject[,];
        target = ub.target;
        targetX = (int)target.transform.position.x;
        targetZ = (int)target.transform.position.z;
        targetPos = target.transform.position;
        currentX = (int)transform.position.x;
        currentZ = (int)transform.position.z;
        currentPos = transform.position;
        checkAdjacent = null;
        calcIndex = 0;

        if (targetPos != currentPos)
        {
            DeterminePriority(priority);
            Calculate();
        }
        else
        {
            Debug.Log("Attempting standstill");
        }
    }

    private void DeterminePriority(int priority) // tells the calculation method "CheckAdjacent()" in what order to retrieve information
    {
        if (priority == 0) // prioritizes 'northmovement' over 'sidemovement'. prioritizes moving towards center of the map over moving outwards
        {
            checkAdjacent += Priority_0;
        }
        if (priority == 1) // prioritizes 'southmovement' over 'sidemovement'. prioritizes moving towards center of the map over moving outwards
        {
            checkAdjacent += Priority_1;
        }
        if (priority == 2)
        {
            checkAdjacent += Priority_2;
        }
    }

    private void Priority_0() // prioritizes 'northmovement' over 'sidemovement'. prioritizes moving towards center of the map over moving outwards
    {
        Vector3[] adjacentPositions = new Vector3[4];
        adjacentPositions[0] = currentPos + Vector3.forward;
        if (currentX < 4.5f) //check if currently on left side
        {
            adjacentPositions[1] = currentPos + Vector3.right;
            adjacentPositions[2] = currentPos + Vector3.left;
        }
        else
        {
            adjacentPositions[1] = currentPos + Vector3.left;
            adjacentPositions[2] = currentPos + Vector3.right;
        }
        adjacentPositions[3] = currentPos + Vector3.back;

        for (int i = 0; i < adjacentPositions.Length; i++)
        {
            int x = (int)adjacentPositions[i].x;
            int z = (int)adjacentPositions[i].z;
            //Debug.Log(x + " " + z);
            if (x >= 0 && x <= 9 && z >= 0 && z <= 9)
            {
                //Debug.Log("coordinate is within grid ");
                if (actualGrid[x, z] == null)
                {
                    calcX = x;
                    calcZ = z;
                    return;
                }
                else if (actualGrid[x, z].tag != "Obstacle" && actualGrid[x, z].tag != "Move Proxy" && actualGrid[x,z].tag != "Unit")
                {
                    //Debug.Log("coordinate is not tagged obstacle");
                    calcX = x;
                    calcZ = z;
                    return;                
                }
                //Debug.Log("but did not give a null in actual grid");
            }
        }
        Debug.Log(gameObject.name + "did not find a valid field around him");
    }

    private void Priority_1()
    {
        calcX = targetX;
        calcZ = targetZ;
    }
    /*
    private void Priority_1() // prioritizes 'northmovement' over 'sidemovement'. prioritizes moving towards center of the map over moving outwards
    {
        Vector3[] adjacentPositions = new Vector3[4];
        adjacentPositions[0] = currentPos + Vector3.forward;
        if (currentX < 4.5f) //check if currently on left side
        {
            adjacentPositions[1] = currentPos + Vector3.right;
            adjacentPositions[2] = currentPos + Vector3.left;
        }
        else
        {
            adjacentPositions[1] = currentPos + Vector3.left;
            adjacentPositions[2] = currentPos + Vector3.right;
        }
        adjacentPositions[3] = currentPos + Vector3.back;

        for (int i = 0; i < adjacentPositions.Length; i++)
        {
            int x = (int)adjacentPositions[i].x;
            int z = (int)adjacentPositions[i].z;
            Debug.Log("checking " + x + " " + z + " as possible path towards target" + "(" + targetX + targetZ + ")");
            if (x >= 0 && x <= 9 && z >= 0 && z <= 9) // check whether it is even in grid
            {
                Debug.Log("coordinate is within grid ");
                if (virtualGrid[x, z] == null) //check if its walkable
                {
                    if (Mathf.Abs(targetZ - z) <= Mathf.Abs(targetZ - currentZ) + pathingLeeway) //check if youre overshooting the goal
                    {
                        Debug.Log("coordinate z is smaller or equal to targetZ");
                        if (Mathf.Abs(targetX - x) <= Mathf.Abs(targetX - currentX) + pathingLeeway) //check if youre walking towards the goal
                        {
                            Debug.Log("coordinate x is closer to target than the current position");
                            calcX = x;
                            calcZ = z;
                            pathingLeeway = 0;
                            return;
                        }
                    }
                }
                else if (virtualGrid[x, z].tag != "Obstacle" && virtualGrid[x, z].tag != "Move Proxy" && virtualGrid[x, z].tag != "Unit")
                {
                    Debug.Log("coordinate is not tagged obstacle");
                    calcX = x;
                    calcZ = z;
                    pathingLeeway = 0;
                    return;
                }
                Debug.Log("coordinate was either an obstacle, beyond target z or outside of grid");
            }
        }
        Debug.LogWarning(gameObject.name + "did not find a valid field around him");
        pathingLeeway++;
        //virtualGrid[currentX, currentZ] = moveProxy;
        //GameManager.instance.Debug_PrintGridToConsole();
        //calcIndex--;
        //pathingFailSafe = true;
    }
    */
    private void Priority_2() // prioritizes 'southmovement' over 'sidemovement'. prioritizes moving towards center of the map over moving outwards
    {
        Vector3[] adjacentPositions = new Vector3[4];
        adjacentPositions[0] = currentPos + Vector3.back;
        if (currentX < 4.5f) //check if currently on left side
        {
            adjacentPositions[1] = currentPos + Vector3.right;
            adjacentPositions[2] = currentPos + Vector3.left;
        }
        else
        {
            adjacentPositions[1] = currentPos + Vector3.left;
            adjacentPositions[2] = currentPos + Vector3.right;
        }
        adjacentPositions[3] = currentPos + Vector3.forward;

        for (int i = 0; i < adjacentPositions.Length; i++)
        {
            int x = (int)adjacentPositions[i].x;
            int z = (int)adjacentPositions[i].z;
            Debug.Log(x + " " + z);
            if (x >= 0 && x <= 9 && z >= 0 && z <= 9)
            {
                Debug.Log("coordinate is within grid ");
                if (actualGrid[x, z] == null)
                {
                    calcX = x;
                    calcZ = z;
                    return;
                }
                else if (actualGrid[x, z].tag != "Obstacle" && actualGrid[x, z].tag != "Move Proxy" && actualGrid[x, z].tag != "Unit")
                {
                    Debug.Log("coordinate is not tagged obstacle");
                    calcX = x;
                    calcZ = z;
                    return;
                }
                Debug.Log("but did not give a null in actual grid");
            }
        }
        Debug.Log(gameObject.name + "did not find a valid field around him");
    }

    public Vector3 ScanForHostiles(int affiliation)
    {
        //actualGrid = gm.gp;
        Debug.Log(gameObject.name + " is scanning for enemies");
        if (affiliation == 0)
        {
            Vector3[] adjacentPositions = new Vector3[4];
            adjacentPositions[0] = currentPos + Vector3.forward;
            if (currentX < 4.5f) //check if currently on left side
            {
                adjacentPositions[1] = currentPos + Vector3.right;
                adjacentPositions[2] = currentPos + Vector3.left;
            }
            else
            {
                adjacentPositions[1] = currentPos + Vector3.left;
                adjacentPositions[2] = currentPos + Vector3.right;
            }
            adjacentPositions[3] = currentPos + Vector3.back;

            for (int i = 0; i < adjacentPositions.Length; i++)
            {
                int x = (int)adjacentPositions[i].x;
                int z = (int)adjacentPositions[i].z;

                if (x >= 0 && x <= 9 && z >= 0 && z <= 9)
                {
                    if (actualGrid[x, z] != null)
                    {
                        if (actualGrid[x, z].tag == "Unit")
                        {
                            if (actualGrid[x, z].GetComponent<UnitBehaviour>().affiliation == 2)
                            {
                                return new Vector3(x, 0, z);
                            }
                        }
                    }
                }
            }
            return new Vector3(99, 99, 99);
        }
        else if (affiliation == 2)
        {
            Vector3[] adjacentPositions = new Vector3[4];
            adjacentPositions[0] = currentPos + Vector3.back;
            if (currentX < 4.5f) //check if currently on left side
            {
                adjacentPositions[1] = currentPos + Vector3.right;
                adjacentPositions[2] = currentPos + Vector3.left;
            }
            else
            {
                adjacentPositions[1] = currentPos + Vector3.left;
                adjacentPositions[2] = currentPos + Vector3.right;
            }
            adjacentPositions[3] = currentPos + Vector3.forward;

            for (int i = 0; i < adjacentPositions.Length; i++)
            {
                int x = (int)adjacentPositions[i].x;
                int z = (int)adjacentPositions[i].z;

                if (x >= 0 && x <= 9 && z >= 0 && z <= 9)
                {
                    if (actualGrid[x, z] != null)
                    {
                        if (actualGrid[x, z].tag == "Unit")
                        {
                            if (actualGrid[x, z].GetComponent<UnitBehaviour>().affiliation == 0)
                                return new Vector3(x, 0, z);
                        }
                    }
                }
            }
        }
        return new Vector3(99, 99, 99);
    }
}
