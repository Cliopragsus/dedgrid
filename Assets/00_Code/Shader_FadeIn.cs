﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shader_FadeIn : MonoBehaviour
{
    public enum TargetRenderer { MeshRenderer, SkinnedMeshRenderer, AnyRenderer };
    public TargetRenderer targetRenderer = TargetRenderer.AnyRenderer;

    private Material mat;
    public float alphaValue = 1;
    //public float shiftSpeed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        if (targetRenderer == TargetRenderer.MeshRenderer)
        {
            if (GetComponent<MeshRenderer>())
            {
                mat = GetComponent<MeshRenderer>().material;
            }
            else
            {
                mat = GetComponentInChildren<MeshRenderer>().material;
            }
        }
        if (targetRenderer == TargetRenderer.SkinnedMeshRenderer)
        {
            if (GetComponent<SkinnedMeshRenderer>())
            {
                mat = GetComponent<SkinnedMeshRenderer>().material;
            }
            else
            {
                mat = GetComponentInChildren<SkinnedMeshRenderer>().material;
            }
        }
        if (targetRenderer == TargetRenderer.AnyRenderer)
        {
            if (GetComponent<Renderer>())
            {
                mat = GetComponent<Renderer>().material;
            }
            else
            {
                mat = GetComponentInChildren<Renderer>().material;
            }
        }
        
        mat.SetFloat("Vector1_F4D56EB", alphaValue);
    }

    // Update is called once per frame
    void Update()
    {
        if (alphaValue > -1)
        {
            //Debug.Log("shaderstuff happening " + alphaValue + gameObject.name);
            alphaValue -= Time.deltaTime / 2;
            mat.SetFloat("Vector1_F4D56EB", alphaValue);
        }
    }
}
